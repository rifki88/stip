
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<?php include APPPATH."/helpers/fungsi_kalender.php";?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-sm-3">
                    <div class="page-title-box">
                        <div class="card" style="padding: 10px; margin-top: 5px; background-color:#29b6f6; color: white">
                            <h4 class="font-size-18">Kelas Umum</h4>
                            <ol class="list-group">
                            <?php $hit = count($ku); for($k=0;$k<$hit;$k++){?>
                                <li class="list-group-item" style="color:black"> 
                                    <?= $ku[$k]['nama_matkul'];?>
                                </li>
                            <?php } ?>
                            </ol>
                        </div>
                        <div class="card" style="padding: 10px;10px; margin-top: 5px; background-color:#29b6f6; color: white">
                            <h4 class="font-size-18">Kelas Akademi</h4>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item active" style="color: white">Semua kelas akademik ditampilkan disini</li>
                            </ol>
                        </div>
                    </div>
                </div>
                

                <div class="col-sm-6">
                    <div class="page-title-box">
                        <h4 class="font-size-18">Bagikan sesuatu di kelas anda</h4>
                        <div class="card text-black">
                            <div class="card-body">
                                <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                    <a href="#" data-toggle="modal" data-target=".modal-materi"><img src="<?= base_url();?>/assets/images/materi.png" alt="Materi" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                    <h6 style="text-align: center;"><a href="#" style="color: #616161">Materi</a></h6>
                                </div>
                                <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                    <a href="#" data-toggle="modal" data-target=".modal-tugas"><img src="<?= base_url();?>/assets/images/tugas.png" alt="Tugas" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                    <h6 style="text-align: center;"><a href="#" style="color: #616161">Tugas</a></h6>
                                </div>
                                <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                    <a href="#" data-toggle="modal" data-target=".modal-kuis"><img src="<?= base_url();?>/assets/images/kuis.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                    <h6 style="text-align: center;"><a href="#" style="color: #616161">Kuis</a></h6>
                                </div>
                                <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                    <a class="mo-mb-2" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <img src="<?= base_url();?>/assets/images/lainnya.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;">
                                    </a>
                                    <h6 style="text-align: center;"><a href="#" style="color: #616161">Lainnya</a></h6>
                                </div>
                            </div>
                            <div class="collapse" id="collapseExample">
                                <div class="card card-body mb-0">
                                    <div class="row">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                            <a href="#" data-toggle="modal" data-target=".modal-info"><img src="<?= base_url();?>/assets/images/info.png" alt="Materi" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                            <h6 style="text-align: center;"><a href="#" style="color: #616161">Info</a></h6>
                                        </div>
                                        <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                            <a href="#" data-toggle="modal" data-target=".modal-tugas"><img src="<?= base_url();?>/assets/images/survey.png" alt="Tugas" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                            <h6 style="text-align: center;"><a href="#" style="color: #616161">Survey</a></h6>
                                        </div>
                                        <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                            <a href="#" data-toggle="modal" data-target=".modal-kuis"><img src="<?= base_url();?>/assets/images/acara.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                            <h6 style="text-align: center;"><a href="#" style="color: #616161">Acara</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="col-sm-12" style="margin-top:30px;">
                            <div class="page-title-box"style="background-color:#29b6f6; padding:10px;">
                            <form method="POST" action="<?= base_url('user/post')?>" enctype="multipart/form-data">
                                <h4 class="font-size-18" style="color:white">Post Information</h4>
                                <div class="card text-black">
                                    <textarea class="card-body" name="description">
                                    
                                    </textarea>
                                    <input type="file" name="foto">
                                </div>
                               
                                <input type="submit" value="Post" class="btn btn-info">
                            </form>
                            </div>
                        </div>
            

                    <div class="page-title-box">
                        <h4 class="font-size-18" style="float: left;"><i class="ti-menu"></i>&nbsp;&nbsp;Timeline</h4>
                        <!--div class="btn-group btn-group-sm" role="group" aria-label="Basic example" style="float: right;">
                            <button type="button" class="btn btn-info waves-effect waves-light">Semua</button>
                            <button type="button" class="btn btn-info waves-effect waves-light">Kelas</button>
                            <button type="button" class="btn btn-info waves-effect waves-light">Berita</button>
                        </div-->
                    </div>

                    <div style="clear: both;">&nbsp;</div>

                    <div class="card text-black">

                    <?php
                    $jumlah =count($post);
                    for($i=0;$i<$jumlah;$i++){
                    ?>
                        <div class="card-body">
                            <div class="media mb-4">
                                <img class="d-flex mr-3 rounded-circle" src="<?= base_url();?>/assets/images/users/user-6.jpg" alt="Generic placeholder image" height="64">
                                <div class="media-body">
                                    <h6 class="mt-2"><?= $post[$i]['user_update']?> <span>memposting di <a href="#">di timeline</a></span></h6>
                                    <small><?= $post[$i]['tanggal_update'];?></small>
                                </div>
                            </div>
                            <p> <?= $post[$i]['description']?> </p>
                            <?php
                            if(empty($post[$i]['foto'])){
                            ?>
                            <img src="<?= base_url();?>/assets/images/small/img-2.jpg" class="img-fluid" alt="Responsive image">
                            <?php }else{ ?>
                            <img src="<?= base_url();?>/assets/upload/post/<?= $post[$i]['foto'];?>" class="img-fluid" alt="Responsive image" style="border:1px solid #000;">
                            <?php  } ?>
                            <div class="mt-3 mb-2" style="border-bottom: 2px solid #e0e0e0;"></div>
                            <!--i class="ti-heart"></i>&nbsp;<a href="#" style="color: #6d6d6d;">Suka</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="ti-comments"></i>&nbsp;<a href="#" style="color: #6d6d6d;">Komentar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="ti-share"></i--> 
                            <!--a href="#" style="color: #6d6d6d;">Bagikan</a-->
                            
                            <?php
                            for($j=0;$j<count($post[$i]['komentar']);$j++){
                            ?>
                            <div class="card text-black bg-secondary mt-1">
                                <div class="card-body">
                                    <div class="media">
                                        <img class="d-flex mr-3 rounded-circle" src="<?= base_url();?>/assets/images/users/user-6.jpg" alt="Generic placeholder image" height="32">
                                        <div class="media-body">
                                            <h6 class=""><?= $post[$i]['komentar'][$j]['user_update']?></h6>
                                            <p><?= $post[$i]['komentar'][$j]['komentar']?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <form method="POST" action="<?= base_url('user/komentarpost/'.$post[$i]['id_post']); ?>">
                            <div class="row">
                                <div class="col-md-1">
                                    <img class="d-flex mr-3 rounded-circle" src="<?= base_url();?>/assets/images/users/user-6.jpg" alt="Generic placeholder image" height="32">
                                </div>
                                <div class="col-md-10">
                                    <textarea id="textarea" name="komentar" class="form-control" maxlength="225" rows="1" placeholder="Tambahkan komentar"></textarea>
                                </div>
                                <div class="col-md-1">
                                    <input type="submit" value=">">
                                </div>
                            </div>
                            </form>
                        </div>
                    <?php } ?>
                    </div>
                    
                </div>

                <p></p>

                <div class="col-sm-3" style="margin-top:30px;">
                    <div class="page-title-box"style="background-color:#29b6f6; padding:10px;">
                        <h4 class="font-size-18" style="color:white">Jadwal Minggu ini</h4>
                        <div class="card text-black">
                            <div class="card-body">
                            
                            
                                        <?php
                                        $tanggal=date("d");
                                        $bulan=date("m");
                                        $tahun=date("Y");
                                        echo buatkalender($tanggal,$bulan,$tahun);
                                        ?>

                            </div>
                        </div>
                    </div>
                    <div class="page-title-box" style="background-color:#29b6f6; padding: 10px;">
                        <h4 class="font-size-18" style="color:white">Tugas belum dikumpulkan <?= $info; ?></h4>
                        <div class="card text-black">
                            <div class="card-body">
                            <ol class="list-group">
                            <?php $hitung = count($tugas); for($l=0;$l<$hitung;$l++){?>
                                <li class="list-group-item" style="color:black"> 
                                    <?= $tugas[$l]['judul'];?>
                                </li>
                            <?php } ?>
                            </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     
            <!-- end page title -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <!-- Modal Materi -->
    <div class="modal fade modal-materi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Atur Materi</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#messages2" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                <span class="d-none d-sm-block">Bagikan Materi</span>   
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="home2" role="tabpanel">
                            <div class="form-group">
                                <label>Dikelas mana Materi akan dibagikan?</label>
                                <!-- <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title=""> -->
                                <select class="form-control select2 select2-hidden-accessible" name="nk" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="3">Pilih Kelas</option>
                                    <optgroup label="Pilih Kelas" data-select2-id="14">
                                        <?php
                                        $h = count($ak);
                                        for($p=0;$p<$h;$p++){
                                        ?>
                                        <option value="<?= $ak[$i]['kode_kelas']?>" data-select2-id="15"><?= $ak[$i]['nama_kelas']?></option>
                                        <?php } ?>
                                    </optgroup>
                                </select>

                            </div>
                            <div class="form-group" data-select2-id="7">
                                <label class="control-label">Di sesi berapa Materi akan dibagikan?</label>
                                <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="3">Pilih Sesi</option>
                                    <optgroup label="Pilih Kelas" data-select2-id="14">
                                        <option value="AK" data-select2-id="15">Kelas Umum</option>
                                        <option value="HI" data-select2-id="16">Kelas Akademik</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="profile2" role="tabpanel">
                            <p class="mb-0">
                            <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control"
                                                value="Masukan Kode Kelas"
                                                disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Sesi 1</label>
                                            <input type="text" class="form-control" value="sesi untuk diskusi">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Materi</label>
                                            <input id="judul" type="text" class="form-control"
                                                placeholder="Masukkan judul materi">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle" id="uploadmateri">
                                        </div>
                            </p>
                        </div>
                        <div class="tab-pane p-3" id="messages2" role="tabpanel">
                            <p class="mb-0">
                               
                            <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangan" class="form-control" maxlength="225"
                                                rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan materi akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i
                                                    class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                onclick="hideTime()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time" id="time" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local"
                                                value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button id="btn_simpan_materi" type="button"
                                            class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal
                                        </button>

                            </p>
                        </div>
                        <div class="tab-pane p-3" id="settings2" role="tabpanel">
                            <p class="mb-0">
                               
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End modal materi -->

    <!-- Modal Tugas -->
    <div class="modal fade modal-tugas" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tugas1" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tugas2" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Atur Tugas</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tugas3" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                <span class="d-none d-sm-block">Bagikan Tugas</span>   
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="tugas1" role="tabpanel">
                            <div class="form-group">
                                <label>Dikelas mana Tugas akan dibagikan?</label>
                                <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                            </div>
                            <div class="form-group" data-select2-id="7">
                                <label class="control-label">Di sesi berapa Tugas akan dibagikan?</label>
                                <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="3">Pilih Sesi</option>
                                    <optgroup label="Pilih Kelas" data-select2-id="14">
                                        <option value="AK" data-select2-id="15">Kelas Umum</option>
                                        <option value="HI" data-select2-id="16">Kelas Khusus</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="tugas2" role="tabpanel">
                            <p class="mb-0">
                               <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control"
                                                value="Masukan Kode Kelas"
                                                disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Sesi 1</label>
                                            <input type="text" class="form-control" value="sesi untuk diskusi">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Materi</label>
                                            <input id="judul" type="text" class="form-control"
                                                placeholder="Masukkan judul materi">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle" id="uploadmateri">
                                        </div>
                            </p>
                        </div>
                        <div class="tab-pane p-3" id="tugas3" role="tabpanel">
                            <p class="mb-0">
                            <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangan" class="form-control" maxlength="225"
                                                rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan materi akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i
                                                    class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                onclick="hideTime()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time" id="time" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local"
                                                value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button id="btn_simpan_materi" type="button"
                                            class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal
                                        </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End modal tugas -->

    <!-- Modal Kuis -->
    <div class="modal fade modal-kuis" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kuis1" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="kuis1" role="tabpanel">
                            <div class="form-group">
                                <label>Dikelas mana Kuis akan dibagikan?</label>
                                <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                            </div>
                            <div class="form-group" data-select2-id="7">
                                <label class="control-label">Di sesi berapa Kuis akan dibagikan?</label>
                                <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="3">Pilih Sesi</option>
                                    <optgroup label="Pilih Kelas" data-select2-id="14">
                                        <option value="AK" data-select2-id="15">Kelas Umum</option>
                                        <option value="HI" data-select2-id="16">Kelas Khusus</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End modal Kuis -->

    <!-- Modal Info -->
    <div class="modal fade modal-info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info1" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#info2" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Atur Info</span> 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#info3" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                <span class="d-none d-sm-block">Bagikan Info</span>   
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="info1" role="tabpanel">
                            <div class="form-group">
                                <label>Dikelas mana Info akan dibagikan?</label>
                                <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="info2" role="tabpanel">
                            <p class="mb-0">
                            <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control"
                                                value="Masukan Kode Kelas"
                                                disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Sesi 1</label>
                                            <input type="text" class="form-control" value="sesi untuk diskusi">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Materi</label>
                                            <input id="judul" type="text" class="form-control"
                                                placeholder="Masukkan judul materi">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle" id="uploadmateri">
                                        </div>
                            </p>
                        </div>
                        <div class="tab-pane p-3" id="info3" role="tabpanel">
                            <p class="mb-0">
                            <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangan" class="form-control" maxlength="225"
                                                rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan materi akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i
                                                    class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                onclick="hideTime()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time" id="time" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local"
                                                value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button id="btn_simpan_materi" type="button"
                                            class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal
                                        </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End modal info -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>document.write(new Date().getFullYear())</script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->