            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                        <div class="row" style="height: 250px; background-color: #81d4fa;">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <br>
                                        <br>
                                        <br>
                                        <figure class="image is-16by9 img-bg mr-5">
                                            <div class="overlay animated fadeIn" style="border: 2px dashed rgb(255, 255, 255); box-sizing: border-box; border-radius: 4px; cursor: pointer;">
                                                <div class="columns is-vcentered" style="margin: 0px; height: 100%;">
                                                    <div class="column has-text-centered">
                                                        <figure class="image is-48x48" style="margin: 0px auto;">
                                                            <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin: 10% 0 0 40%;">
                                                                <g opacity="0.8">
                                                                    <path d="M13.457 36.0838C10.5228 36.0838 7.90957 34.2052 6.96084 31.406L6.89772 31.1938C6.67481 30.4512 6.57879 29.8301 6.57879 29.2057V16.7062L2.13457 31.557C1.56251 33.7405 2.86576 36.0066 5.05262 36.6089L33.4039 44.2022C33.7564 44.2962 34.111 44.3371 34.4574 44.3371C36.2837 44.3371 37.9529 43.1272 38.4175 41.3392L40.0679 36.0865H13.457V36.0838Z" fill="white"></path> 
                                                                    <path d="M18.4979 16.8325C20.5196 16.8325 22.164 15.1881 22.164 13.1665C22.164 11.1448 20.5196 9.50043 18.4979 9.50043C16.4762 9.50043 14.8319 11.1448 14.8319 13.1665C14.8319 15.1881 16.4762 16.8325 18.4979 16.8325Z" fill="#FAFAFA"></path> 
                                                                    <path d="M41.4154 4H13.9135C11.3862 4 9.32892 6.05728 9.32892 8.58456V28.7518C9.32892 31.279 11.3862 33.3363 13.9135 33.3363H41.4154C43.9427 33.3363 46 31.279 46 28.7518V8.58456C46 6.05459 43.9427 4 41.4154 4ZM13.9135 7.66603H41.4154C41.9217 7.66603 42.3313 8.07561 42.3313 8.58187V21.5983L36.5395 14.841C35.9258 14.1205 35.0348 13.7358 34.0807 13.7136C33.1319 13.719 32.2383 14.14 31.6299 14.8719L24.8236 23.0452L22.6065 20.8282C21.353 19.5746 19.3118 19.5746 18.0582 20.8282L12.9976 25.8888V8.58187C12.9976 8.07561 13.4072 7.66603 13.9135 7.66603Z" fill="white"></path>
                                                                </g>
                                                            </svg>
                                                        </figure> 
                                                        <p style="text-align:center; padding: 10px; color: #ffffff;">Tambahkan Gambar untuk Thumbnail Kelas</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="col-sm-6" style="color: #ffffff;">
                                        <br><br>
                                        <div class="row">
                                            <h3>Kelas Manajemen Informatika <?= $ambil[0]['nama_matkul']?></h3>
                                            <!-- <h5></h5> -->
                                        </div>
                                        <br><br><br>
                                        <div class="row" style="vertical-align: bottom;">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Kode Kelas
                                                </div>
                                                <div class="row">
                                                    <?= urlencode(base64_decode($this->input->get('id'))); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Jumlah Anggota
                                                </div>
                                                <div class="row">
                                                    <?= $memberkelas; ?> Anggota
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Pengelola
                                                </div>
                                                <div class="row">
                                                    <?= $ambil[0]['user_update']?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" style="float: right; padding-top: 10%; color: #ffffff;">Ganti Cover</a>
                                            </div>
                                        </div>
                                        <br><br><br><br><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" style="float: right; padding-top: 10%; color: #ffffff;"><span>Bagikan</span><br>Tampilkan PIN/LINK&nbsp;&nbsp;<i class="ti ti-share"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- start page title -->
                        <div class="row">
                            <div class="container-fluid">
                            <div class="row">

                            <div class="col-sm-3">
                                <div class="page-title-box">
                                    <div class="row" style="padding: 10px;">
                                        <div class="col-md-12">
                                            <a href="#"><b><span class="icon"><svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg" style="width: 20px;"><path d="M5.08366 8.75L1.33366 5M1.33366 5L5.08366 1.25M1.33366 5L14.667 5" stroke="#039be5" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"></path></svg></span>&nbsp;&nbsp;Kembali</b></a>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 10px;"> 
                                        <a href="<?= base_url();?>user/informasi_kelas?id=<?php echo $data_isi[0]['id']; ?>">Informasi Kelas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/diskusi_kelas?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Diskusi</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/sesi_kelas?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Sesi Pembelajaran</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/tugas?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Tugas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/quiz?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Quiz</a>
                                    </div>
                                    <div class="row" style="border-radius: 10px; background: #e2e7ff!important; width: 100%; padding: 10px;">
                                        <a href="<?= base_url();?>user/berkas?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Berkas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/anggota?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Anggota</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url();?>user/laporan?id=<?php echo urlencode(base64_encode($data_isi[0]['id'])); ?>">Laporan</a>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-sm-6">
                                <!-- <div class="page-title-box">
                                    <h4 class="font-size-18">Bagikan sesuatu di kelas anda</h4>
                                    <div class="card text-black">
                                        <div class="card-body">
                                            <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                                <a href="#" data-toggle="modal" data-target=".modal-materi"><img src="assets/images/materi.png" alt="Materi" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                <h6 style="text-align: center;"><a href="#" style="color: #616161">Materi</a></h6>
                                            </div>
                                            <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                                <a href="#" data-toggle="modal" data-target=".modal-tugas"><img src="assets/images/tugas.png" alt="Tugas" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                <h6 style="text-align: center;"><a href="#" style="color: #616161">Tugas</a></h6>
                                            </div>
                                            <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                                <a href="#" data-toggle="modal" data-target=".modal-kuis"><img src="assets/images/kuis.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                <h6 style="text-align: center;"><a href="#" style="color: #616161">Kuis</a></h6>
                                            </div>
                                            <div class="col-md-3 hovericon" style="float: left; padding: 10px;">
                                                <a class="mo-mb-2" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                    <img src="assets/images/lainnya.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;">
                                                </a>
                                                <h6 style="text-align: center;"><a href="#" style="color: #616161">Lainnya</a></h6>
                                            </div>
                                        </div>
                                        <div class="collapse" id="collapseExample">
                                            <div class="card card-body mb-0">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                                        <a href="#" data-toggle="modal" data-target=".modal-info"><img src="assets/images/info.png" alt="Materi" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                        <h6 style="text-align: center;"><a href="#" style="color: #616161">Info</a></h6>
                                                    </div>
                                                    <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                                        <a href="#" data-toggle="modal" data-target=".modal-survey"><img src="assets/images/survey.png" alt="Tugas" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                        <h6 style="text-align: center;"><a href="#" style="color: #616161">Survey</a></h6>
                                                    </div>
                                                    <div class="col-md-3 hovericon" style="padding-top: 10px; padding-bottom: 10px;">
                                                        <a href="#" data-toggle="modal" data-target=".modal-acara"><img src="assets/images/acara.png" alt="Kuis" width="50" style="margin-left: auto; margin-right: auto; display: block;"></a>
                                                        <h6 style="text-align: center;"><a href="#" style="color: #616161">Acara</a></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="page-title-box">
                                    <h4 class="font-size-18" style="float: left;"><i class="ti-menu"></i>&nbsp;&nbsp;Berkas</h4>
                                    <!-- <div class="btn-group btn-group-sm" role="group" aria-label="Basic example" style="float: right;">
                                        <button type="button" class="btn btn-info waves-effect waves-light">Semua</button>
                                        <button type="button" class="btn btn-info waves-effect waves-light">Kelas</button>
                                        <button type="button" class="btn btn-info waves-effect waves-light">Berita</button>
                                    </div> -->
                                </div>

                                <div style="clear: both;">&nbsp;</div>

                                <div class="card text-black">
                                    <div class="card-body">
                                    <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Berkas</th>
                                                            <th>Berkas</th>
                                                            <th>Thumbnail</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        $no=0;
                                                        $jumlah = count($files);
                                                        for($i=0;$i<$jumlah;$i++){
                                                        ?>
                                                         <tr>
                                                            <td><?= $files[$i]['id']?></td>
                                                            <td><?= $files[$i]['berkas']?></td>
                                                            <td> <img src="<?= base_url('assets/images/gbr.png')?>" width="100"></td>
                                                        </tr>
                                                        <?php }
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                         
                            <div class="col-sm-3">
                                <div class="page-title-box">
                                    <h4 class="font-size-18">Tugas belum dikumpulkan</h4>
                                    <div class="card text-black">
                                        <div class="card-body">
                                            <p>Tidak ada tugas</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            </div>
                        </div>     
                        <!-- end page title -->

                    
                </div>
                <!-- End Page-content -->

                <!-- Modal Materi -->
                <div id="modal-materi" class="modal fade modal-materi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Materi di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Atur Materi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#messages2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Materi</span>   
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="home2" role="tabpanel">
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa materi akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" id="id_sesi" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value="">Pilih Sesi</option>
                                                <option value="AK" data-select2-id="15">Sesi 1</option>
                                                <option value="HI" data-select2-id="16">Sesi 2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Topik sesi</label>
                                            <input type="hidden" id="id_kelas" name="id_kelas" value="<?= $this->input->get('id');?>">
                                            <input type="text" class="form-control" placeholder="Masukkan topik sesi">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control" value="<?= $ambil[0]['nama_matkul'].'/'.$ambil[0]['kode_kelas']?>" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Sesi 1</label>
                                            <input type="text" class="form-control" value="sesi untuk diskusi">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Materi</label>
                                            <input id="judul" type="text" class="form-control" placeholder="Masukkan judul materi">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="messages2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangan" class="form-control" maxlength="225" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan materi akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time" id="time" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button id="btn_simpan_materi" type="button" class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal materi -->

                <!-- Modal Tugas -->
                <div id="modal-tugas" class="modal fade modal-tugas" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tugas1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tugas2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Atur Tugas</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tugas3" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Tugas</span>   
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="tugas1" role="tabpanel">
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa tugas akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" id="id_sesi" tabindex="-1" aria-hidden="true">
                                                <option value="">Pilih Sesi</option>
                                                <option value="AK" data-select2-id="15">Sesi 1</option>
                                                <option value="HI" data-select2-id="16">Sesi 2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Topik sesi</label>
                                            <input type="text" class="form-control" id="topik" placeholder="Masukkan topik sesi">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="tugas2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Kode Kelas</label>
                                            <input type="text" id="id_kelas" class="form-control" value="<?= base64_decode(urldecode($this->input->get('id')))?>" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Sesi 1</label>
                                            <input type="text" class="form-control" value="Test sesi untuk diskusi" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Tugas</label>
                                            <input id="judultugas" type="text" class="form-control" placeholder="Masukkan judul tugas">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input id="berkas" type="file" class="filestyle">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="tugas3" role="tabpanel">
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangantugas" class="form-control" maxlength="225" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Tambahkan batas waktu penyerahan?</label><br>
                                            <input type="checkbox" id="switch3" switch="bool" onclick="mySerah()"/>
                                            <label for="switch3" data-on-label="Ya" data-off-label="Tidak"></label>
                                        </div>
                                        <div class="form-group penyerahan" id="penyerahan" style="display: none;">
                                            <label>Pilih tanggal dan waktu penyerahan</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00">
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan tugas akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime2()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time2" id="time2" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <input type="hidden" id="key" value="<?= $this->input->get('id');?>">
                                        <button id="btn_simpan_tugas" type="button" class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal tugas -->

                <!-- Modal Kuis -->
                <div class="modal fade modal-kuis" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kuis1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="kuis1" role="tabpanel">
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa kuis akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value="">Pilih Sesi</option>
                                                <option value="AK" data-select2-id="15">Sesi 1</option>
                                                <option value="HI" data-select2-id="16">Sesi 2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Topik sesi</label>
                                            <input type="text" class="form-control" placeholder="Masukkan topik sesi">
                                        </div>
                                        <button type="button" class="btn btn-info waves-effect waves-light mr-2"> Buat Kuis </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal Kuis -->

                <!-- Modal Info -->
                <div id="modal-info" class="modal fade modal-info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Atur Info</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#info2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Info</span> 
                                        </a>
                                    </li>
                                </ul>
                                <form entype="multipart/form-data">
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="info1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Kelas</label>
                                            <input id="get_nama" type="text" class="form-control" value="Kelas Manajemen Informatika (MAI01)" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Info</label>
                                            <input id="judul" type="text" class="form-control" placeholder="Masukkan judul info">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input id="berkas" type="file" class="filestyle">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="info2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="keterangan" class="form-control" maxlength="225" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan info akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime3()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time3" id="time3" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="waktu_dibagikan">
                                        </div>
                                        <button id="btn_simpan_info" type="button" class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal info -->

                <!-- Modal Survey -->
                <div class="modal fade modal-survey" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#survey1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Atur Survey</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#survey2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Survey</span> 
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="survey1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control" value="Kelas Manajemen Informatika (MAI01)" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Survei</label>
                                            <input type="text" class="form-control" placeholder="Masukkan judul survei">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle">
                                        </div>
                                        <div class="form-group">
                                            <label class="d-block mb-3">Tipe :</label>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="radioSurvei1" name="outer-group[0][radioSurvei1]" class="custom-control-input">
                                                <label class="custom-control-label" for="radioSurvei1">Satu Pilihan</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="radioSurvei1" name="outer-group[0][radioSurvei1]" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline2">Banyak Pilihan</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Opsi</label>
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <input type="text" class="form-control" placeholder="Tambahkan opsi">
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-secondary waves-effect waves-light">X</button>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-secondary waves-effect waves-light"><i class="ti ti-plus"></i>&nbsp;Tambahkan Opsi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="survey2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="textarea" class="form-control" maxlength="225" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan info akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime4()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time4" id="time4" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button type="button" class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal survey -->

                <!-- Modal Acara -->
                <div class="modal fade modal-acara" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#acara1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Atur Acara</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#acara2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Acara</span> 
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="acara1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Kelas</label>
                                            <input type="text" class="form-control" value="Kelas Manajemen Informatika (MAI01)" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Acara</label>
                                            <input type="text" class="form-control" placeholder="Masukkan judul acara">
                                        </div>
                                        <div class="form-group">
                                            <label>Berkas (Dokumen, Gambar, Video)</label>
                                            <input type="file" class="filestyle">
                                        </div>
                                        <div class="form-group">
                                            <label>Dimulai pada :</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime5()"> <i class="ti ti-alarm-clock"></i> Tampilkan waktu selesai </button>
                                        <br><br>
                                        <div class="form-group time5" id="time5" style="display: none;">
                                            <label>Selesai pada :</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <div class="form-group">
                                            <label>Lokasi</label>
                                            <input type="text" class="form-control" placeholder="Masukkan lokasi acara">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="acara2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea id="textarea" class="form-control" maxlength="225" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Kapan acara akan dibagikan?</label><br>
                                            <button type="button" class="btn btn-info waves-effect waves-light"> <i class="ti ti-check"></i> Sekarang</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light" onclick="hideTime6()"> <i class="ti ti-alarm-clock"></i> Nanti</button>
                                        </div>
                                        <div class="form-group time6" id="time6" style="display: none;">
                                            <label>Pilih tanggal dan waktu</label><br>
                                            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                        </div>
                                        <button type="button" class="btn btn-info waves-effect waves-light mr-2"> Bagikan </button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal acara -->

                <!-- Modal Ubah -->
                <div class="modal fade modal-ubah" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Ubah Informasi Kelas :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nama Mata Kuliah :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                </div>
                                <div class="form-group">
                                    <label>Nama Kelas :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                </div>
                                <div class="form-group">
                                    <label>Deskripsi :</label>
                                    <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="Jelaskan secara singkat maksud dari pembuatan kelas ini"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="d-block mb-3">Tipe :</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="outer-group[0][customRadioInline1]" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Publik</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="outer-group[0][customRadioInline1]" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Pribadi</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Apa yang akan peserta pelajari :</label>
                                    <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="Jelaskan secara singkat apa yang akan peserta pelajari"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-secondary ml-2">Batal</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                © <script>document.write(new Date().getFullYear())</script> STIP JAKARTA
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Script data untuk diskusi kelas -->
        <script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>


        <script type="text/javascript">

        $(document).ready(function(){
    
            //Simpan bagikan materi
            $('#btn_simpan_materi').on('click',function(){
                //alert('test');
                var id_kelas            = $('#id_kelas').val();
                var judul               = $('#judul').val();
                var keterangan          = $('#keterangan').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('diskusi_kelas/SaveMateri'); ?>",
                    async : true,
                    dataType : 'json',
                    data: { 
                            id_kelas: id_kelas, 
                            judul: judul, 
                            keterangan: keterangan
                        },
                    success: function(data){
                    $('#judul').val("");
                    $('#keterangan').val("");
                    $('#modal-materi').modal('hide');
                    window.location.href = "http://localhost/stip/user/diskusi_kelas?id="+id_kelas;
                    }
                });
                return false;
            });

            //Simpan bagikan tugas
            $('#btn_simpan_tugas').on('click',function(){
                var fileupload          = $('#berkas').prop('files')[0];
                var judul               = $('#judultugas').val();
                var keterangan          = $('#keterangantugas').val();
                var key                 = $('#key').val();
                var id_sesi             = $('#id_sesi').val();
                var id_kelas            = $('#id_kelas').val();
                var formData = new FormData();
                formData.append('judul', judul);
                formData.append('keterangan', keterangan);
                formData.append('berkas', fileupload);  
                formData.append('id_sesi', id_sesi);  
                formData.append('id_kelas', id_kelas); 
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('diskusi_kelas/SaveTugas'); ?>",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data){
                    $('#judul').val("");
                    $('#keterangan').val("");
                    $('#modal-tugas').modal('hide');
                    window.location.href = "http://localhost/sekolah/stip/user/diskusi_kelas?id="+key;
                    //console.log(data);
                    }
                });
                return false;
            });

            //Simpan bagikan info
            $('#btn_simpan_info').on('click',function(){
                var judul               = $('#judul').val();
                var keterangan          = $('#keterangan').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('diskusi_kelas/SaveInfo'); ?>",
                    async : true,
                    dataType : 'json',
                    data: { 
                            judul: judul, 
                            keterangan: keterangan
                        },
                    success: function(data){
                    $('#judul').val("");
                    $('#keterangan').val("");
                    $('#modal-info').modal('hide');
                    window.location.href = "http://localhost/stip/user/diskusi_kelas";
                    }
                });
                return false;
            });


        });

        function hideTime() {
            var x = document.getElementById("time");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideTime2() {
            var x = document.getElementById("time2");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideTime3() {
            var x = document.getElementById("time3");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideTime4() {
            var x = document.getElementById("time4");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideTime5() {
            var x = document.getElementById("time5");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideTime6() {
            var x = document.getElementById("time6");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function mySerah() {
            var checkBox = document.getElementById("switch3");
            var text = document.getElementById("penyerahan");
            if (checkBox.checked == true){
                text.style.display = "block";
            } else {
                text.style.display = "none";
            }
        }
        </script>