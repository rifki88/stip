  <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                        <div class="row" style="height: 250px; background-color: #81d4fa;">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <br>
                                        <br>
                                        <br>
                                        <figure class="image is-16by9 img-bg mr-5">
                                            <div class="overlay animated fadeIn" style="border: 2px dashed rgb(255, 255, 255); box-sizing: border-box; border-radius: 4px; cursor: pointer;">
                                                <div class="columns is-vcentered" style="margin: 0px; height: 100%;">
                                                    <div class="column has-text-centered">
                                                        <figure class="image is-48x48" style="margin: 0px auto;">
                                                            <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin: 10% 0 0 40%;">
                                                                <g opacity="0.8">
                                                                    <path d="M13.457 36.0838C10.5228 36.0838 7.90957 34.2052 6.96084 31.406L6.89772 31.1938C6.67481 30.4512 6.57879 29.8301 6.57879 29.2057V16.7062L2.13457 31.557C1.56251 33.7405 2.86576 36.0066 5.05262 36.6089L33.4039 44.2022C33.7564 44.2962 34.111 44.3371 34.4574 44.3371C36.2837 44.3371 37.9529 43.1272 38.4175 41.3392L40.0679 36.0865H13.457V36.0838Z" fill="white"></path> 
                                                                    <path d="M18.4979 16.8325C20.5196 16.8325 22.164 15.1881 22.164 13.1665C22.164 11.1448 20.5196 9.50043 18.4979 9.50043C16.4762 9.50043 14.8319 11.1448 14.8319 13.1665C14.8319 15.1881 16.4762 16.8325 18.4979 16.8325Z" fill="#FAFAFA"></path> 
                                                                    <path d="M41.4154 4H13.9135C11.3862 4 9.32892 6.05728 9.32892 8.58456V28.7518C9.32892 31.279 11.3862 33.3363 13.9135 33.3363H41.4154C43.9427 33.3363 46 31.279 46 28.7518V8.58456C46 6.05459 43.9427 4 41.4154 4ZM13.9135 7.66603H41.4154C41.9217 7.66603 42.3313 8.07561 42.3313 8.58187V21.5983L36.5395 14.841C35.9258 14.1205 35.0348 13.7358 34.0807 13.7136C33.1319 13.719 32.2383 14.14 31.6299 14.8719L24.8236 23.0452L22.6065 20.8282C21.353 19.5746 19.3118 19.5746 18.0582 20.8282L12.9976 25.8888V8.58187C12.9976 8.07561 13.4072 7.66603 13.9135 7.66603Z" fill="white"></path>
                                                                </g>
                                                            </svg>
                                                        </figure> 
                                                        <p style="text-align:center; padding: 10px; color: #ffffff;">Tambahkan Gambar untuk Thumbnail Kelas</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="col-sm-6" style="color: #ffffff;">
                                        <br><br>
                                        <div class="row">
                                            <h3>Kelas Manajemen Informatika <?= $api[0]['nama_kelas']?></h3>
                                            <!-- <h5></h5> -->
                                        </div>
                                        <br><br><br>
                                        <div class="row" style="vertical-align: bottom;">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Kode Kelas
                                                </div>
                                                <div class="row">
                                                <?= $api[0]['kode_kelas']?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Jumlah Anggota
                                                </div>
                                                <div class="row">
                                                    0 Anggota
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    Pengelola
                                                </div>
                                                <div class="row">
                                                    Administrator
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" style="float: right; padding-top: 10%; color: #ffffff;">Ganti Cover</a>
                                            </div>
                                        </div>
                                        <br><br><br><br><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" style="float: right; padding-top: 10%; color: #ffffff;"><span>Bagikan</span><br>Tampilkan PIN/LINK&nbsp;&nbsp;<i class="ti ti-share"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- start page title -->
                        <div class="row">
                            <div class="container-fluid">
                            <div class="row">

                            <div class="col-sm-3">
                                <div class="page-title-box">
                                    <div class="row" style="padding: 10px;">
                                        <div class="col-md-12">
                                            <a href="#"><b><span class="icon"><svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg" style="width: 20px;"><path d="M5.08366 8.75L1.33366 5M1.33366 5L5.08366 1.25M1.33366 5L14.667 5" stroke="#039be5" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"></path></svg></span>&nbsp;&nbsp;Kembali</b></a>
                                        </div>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/detailkelasakademik/'.$this->uri->segment(3))?>">Informasi Kelas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/diskusiakademik/'.$this->uri->segment(3))?>">Diskusi</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/sesiakademik/'.$this->uri->segment(3))?>">Sesi Pembelajaran</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/tugasakademik/'.$this->uri->segment(3))?>">Tugas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/quizakademik/'.$this->uri->segment(3))?>">Quiz</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/berkasakademik/'.$this->uri->segment(3))?>">Berkas</a>
                                    </div>
                                    <div class="row" style="padding: 10px;">
                                        <a href="<?= base_url('user/anggotaakademik/'.$this->uri->segment(3))?>">Anggota</a>
                                    </div>
                                    <div class="row"  style="border-radius: 10px; background: #e2e7ff!important; width: 100%; padding: 10px;">
                                        <a href="<?= base_url('user/laporanakademik/'.$this->uri->segment(3))?>">Laporan</a>
                                    </div>
                                   
                                </div>
                            </div>
                            
                            <div class="col-sm-9" class="tab-pane active" id="Informasi_kelas">
                                <div class="page-title-box">
                                    <div class="card text-black">
                                        <div class="card-body">

                                        <!-- <div class="table-responsive">
                                          <table class="table">
                                              <thead>
                                                  <tr>
                                                      <th>ID Kelas</th>
                                                      <th>Nama Kelas</th>
                                                      <th>Daya Tampung</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>ID Kelas</td>
                                                      <td>Nama Kelas</td>
                                                      <td>Daya Tampung</td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                       -->

                                       <h3 align="center">Belum Ada Laporan </h3>


                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>

                            </div>
                            </div>
                        </div>     
                        <!-- end page title -->

                    
                </div>
                <!-- End Page-content -->

                <!-- Modal Materi -->
                <div class="modal fade modal-materi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Atur Materi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#messages2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Materi</span>   
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="home2" role="tabpanel">
                                        <div class="form-group">
                                            <label>Dikelas mana Materi akan dibagikan?</label>
                                            <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                        </div>
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa Materi akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option data-select2-id="3">Pilih Sesi</option>
                                                <optgroup label="Pilih Kelas" data-select2-id="14">
                                                    <option value="AK" data-select2-id="15">Kelas Umum</option>
                                                    <option value="HI" data-select2-id="16">Kelas Khusus</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                        <p class="mb-0">
                                          
                                        </p>
                                    </div>
                                    <div class="tab-pane p-3" id="messages2" role="tabpanel">
                                        <p class="mb-0">
                                           
                                        </p>
                                    </div>
                                    <div class="tab-pane p-3" id="settings2" role="tabpanel">
                                        <p class="mb-0">
                                          
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal materi -->

                <!-- Modal Tugas -->
                <div class="modal fade modal-tugas" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tugas1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tugas2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Atur Tugas</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tugas3" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Tugas</span>   
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="tugas1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Dikelas mana Tugas akan dibagikan?</label>
                                            <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                        </div>
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa Tugas akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option data-select2-id="3">Pilih Sesi</option>
                                                <optgroup label="Pilih Kelas" data-select2-id="14">
                                                    <option value="AK" data-select2-id="15">Kelas Umum</option>
                                                    <option value="HI" data-select2-id="16">Kelas Khusus</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="tugas2" role="tabpanel">
                                        <p class="mb-0">
                                          
                                        </p>
                                    </div>
                                    <div class="tab-pane p-3" id="tugas3" role="tabpanel">
                                        <p class="mb-0">
                                         
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal tugas -->

                <!-- Modal Kuis -->
                <div class="modal fade modal-kuis" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kuis1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="kuis1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Dikelas mana Kuis akan dibagikan?</label>
                                            <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                        </div>
                                        <div class="form-group" data-select2-id="7">
                                            <label class="control-label">Di sesi berapa Kuis akan dibagikan?</label>
                                            <select class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option data-select2-id="3">Pilih Sesi</option>
                                                <optgroup label="Pilih Kelas" data-select2-id="14">
                                                    <option value="AK" data-select2-id="15">Kelas Umum</option>
                                                    <option value="HI" data-select2-id="16">Kelas Khusus</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal Kuis -->

                <!-- Modal Info -->
                <div class="modal fade modal-info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Bagikan Sesuatu di Kelas Anda :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Pilih Kelas & Sesi</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#info2" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Atur Info</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#info3" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Bagikan Info</span>   
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="info1" role="tabpanel">
                                        <div class="form-group">
                                            <label>Dikelas mana Info akan dibagikan?</label>
                                            <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="info2" role="tabpanel">
                                        <p class="mb-0">
                                          
                                        </p>
                                    </div>
                                    <div class="tab-pane p-3" id="info3" role="tabpanel">
                                        <p class="mb-0">
                                          
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End modal info -->

                <!-- Modal Ubah -->
                <div class="modal fade modal-ubah" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">Ubah Informasi Kelas :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nama Mata Kuliah :
                                    
                                    </label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                </div>
                                <div class="form-group">
                                    <label>Nama Kelas :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" value="Pilih Kelas" data-colorpicker-id="1" data-original-title="" title="">
                                </div>
                                <div class="form-group">
                                    <label>Deskripsi :</label>
                                    <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="Jelaskan secara singkat maksud dari pembuatan kelas ini"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="d-block mb-3">Tipe :</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="outer-group[0][customRadioInline1]" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Publik</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="outer-group[0][customRadioInline1]" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Pribadi</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Apa yang akan peserta pelajari :</label>
                                    <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="Jelaskan secara singkat apa yang akan peserta pelajari"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-secondary ml-2">Batal</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                © <script>document.write(new Date().getFullYear())</script> STIP JAKARTA
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->