<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Kelas</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">


                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="akademik" role="tabpanel">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>KELAS AKADEMIK</h5>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title mt-0">Gabung ke Kelas :</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="<?= base_url('user/insert_join_kelas')?>">
                                            <div class="form-group">
                                                <label>PIN :</label>
                                                <input type="text"
                                                    class="colorpicker-default form-control colorpicker-element"
                                                    data-colorpicker-id="1" data-original-title="" title="" name="pin"
                                                    placeholder="e.g ABCDE">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Buat</button>
                                            <button type="button" class="btn btn-secondary">Tutup</button>
                                        </form>
                                        </div>
                                    </div><!-- /.modal-content -->

                                </div>
                            </div>
                            </p>
                        </div>
                      
                    </div>
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>



<!-- tutup script buat kelas -->