<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Kelas</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#akademik" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">AKADEMIK</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#umum" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">UMUM</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="akademik" role="tabpanel">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>KELAS AKADEMIK</h5>
                                    <div class="col-md-12">
                                    <form method="POST" action="<?= base_url('user/search')?>">
                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="search" name="nama_kelas" id="example-search-input" placeholder="Cari ...">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="idperiode">
                                                    <option>Pilih Periode</option>
                                                    <?php
                                                    for($p=20000;$p<20200;$p++){
                                                        echo"<option value='".$p."'>".$p."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">    
                                            <button type="submit" class="btn btn-success"><i class="ti-filter"></i></button>
                                            </div>
                                        </div>
                                        </form>
                                        <table style="width: 100%; font-size: 14px;" class="table table-striped border"
                                            >
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>ID Kelas</th>
                                                    <th>Nama Kelas</th>
                                                    <th>Periode</th>
                                                    <th>Daya Tampung</th>
                                                    <th>Unit</th>
                                                    <th>Kurikulum</th>
                                                    <th>MK</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px">
                                                <?php
                                                $no=1;
                                                            $jumlah = count($api);
                                                            for($i=0;$i<$jumlah;$i++){
                                                        ?>
                                                <tr>
                                                    <td><?= $no ?></th>
                                                    <td><a href="<?= base_url('user/detailkelasakademik/'.$api[$i]['id'])?>"><?= $api[$i]['kode_kelas'] ?></a></th>
                                                    <td><?= $api[$i]['nama_kelas'] ?></th>
                                                    <td><?= $api[$i]['idperiode'] ?></th>
                                                    <td><?= $api[$i]['daya_tampung'] ?></th>
                                                    <td><?= $api[$i]['idunit'] ?></th>
                                                    <td><?= $api[$i]['idkurikulum'] ?></th>
                                                    <td><?= $api[$i]['idmk'] ?></th>

                                                </tr>
                                                <?php
                                                $no++;
                                                            }
                                                        ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                        <div class="tab-pane p-3" id="umum" role="tabpanel">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>KELAS YANG TELAH DIBUAT</h5>
                                    <P>Kelas umum yang anda buat dan dapat anda kelola</P>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" data-toggle="modal" data-target=".modal-lainnya"
                                        style="padding: 5px; float: right; color: #616161;"><b>LAINNYA</b></a>
                                    <a href="#" data-toggle="modal" data-target=".modal-gabung"
                                        style="padding: 5px; float: right; color: #616161;"><b>GABUNG KE
                                            KELAS&nbsp;&nbsp;|</b></a>
                                    <a href="#" data-toggle="modal" data-target=".modal-buat"
                                        style="padding: 5px; float: right; color: #616161;"><b>BUAT
                                            KELAS&nbsp;&nbsp;|</b></a>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table style="width: 100%; font-size: 14px;" class="table table-striped border"
                                    id="listKelasTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Nama</th>
                                            <th style="width: 20%">Pengelola</th>
                                            <th style="width: 20%">Peserta</th>
                                            <th style="width: 20%">Jadwal</th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 12px" id="listKelas2">
                                        <!-- Untuk menampilkan datanya, menggunakan JQuery + AJAX -->
                                    </tbody>
                                </table>
                            </div>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <!-- Modal Info save -->
    <form class="form-horizontal">
        <div class="modal fade modal-buat" id="addKelasModal" tabindex="-1" role="dialog"
            aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">Buat Kelas Baru :</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Mata Kuliah :</label>
                            <input type="text" id="nama" style="text-transform: uppercase"
                                class="colorpicker-default form-control colorpicker-element"
                                placeholder="Nama Mata Pelajaran" data-colorpicker-id="1" data-original-title=""
                                title="" requered>
                        </div>
                        <div class="form-group">
                            <label>Kode Kelas :</label>
                            <input type="text" id="id_kelas" style="text-transform: uppercase"
                                class="colorpicker-default form-control colorpicker-element" placeholder="Kode Kelas"
                                data-colorpicker-id="1" data-original-title="" title="">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi :</label>
                            <textarea id="deskripsi" class="form-control" maxlength="225" rows="3"
                                placeholder="Jelaskan secara singkat maksud dari pembuatan kelas ini"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="d-block mb-3">Tipe :</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="outer-group[0][customRadioInline1]"
                                    class="custom-control-input" value="Publik">
                                <label class="custom-control-label" for="customRadioInline1">Publik</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="outer-group[0][customRadioInline1]"
                                    class="custom-control-input" value="Pribadi">
                                <label class="custom-control-label" for="customRadioInline2">Pribadi</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Apa yang akan peserta pelajari :</label>
                            <textarea id="keterangan" class="form-control" maxlength="225" rows="3"
                                placeholder="Jelaskan secara singkat apa yang akan peserta pelajari"></textarea>
                        </div>
                        <button id="btn_simpan_klsu" class="btn btn-primary">Buat</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </form>
    <!-- /.modal -->


    <!-- Modal Info edit -->
    <form class="form-horizontal">
        <div class="modal fade modal-buat" id="editKelasModal" tabindex="-1" role="dialog"
            aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">Edit Kelas : <span id="nama_kelas_umum"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Mata Kuliah :</label>
                            <input type="text" id="get_nama" style="text-transform: uppercase"
                                class="colorpicker-default form-control colorpicker-element"
                                placeholder="Nama Mata Pelajaran" data-colorpicker-id="1" data-original-title=""
                                title="">
                        </div>
                        <div class="form-group">
                            <label>Kode Kelas :</label>
                            <input type="text" id="get_id_kelas" style="text-transform: uppercase"
                                class="colorpicker-default form-control colorpicker-element" placeholder="Kode Kelas"
                                data-colorpicker-id="1" data-original-title="" title="">
                            <input type="text" id="get_id" class="colorpicker-default form-control colorpicker-element"
                                placeholder="Kode Kelas" data-colorpicker-id="1" data-original-title="" title="" hidden>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi :</label>
                            <textarea id="get_deskripsi" class="form-control" maxlength="225" rows="3"
                                placeholder="Jelaskan secara singkat maksud dari pembuatan kelas ini"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="d-block mb-3">Tipe :</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="get_customRadioInline1"
                                    name="outer-group[0][customRadioInline1]" class="custom-control-input"
                                    value="Publik">
                                <label class="custom-control-label" for="get_customRadioInline1">Publik</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="get_customRadioInline2"
                                    name="outer-group[0][customRadioInline1]" class="custom-control-input"
                                    value="Pribadi">
                                <label class="custom-control-label" for="get_customRadioInline2">Pribadi</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Apa yang akan peserta pelajari :</label>
                            <textarea id="get_keterangan" class="form-control" maxlength="225" rows="3"
                                placeholder="Jelaskan secara singkat apa yang akan peserta pelajari"></textarea>
                        </div>
                        <button id="btn_ubah_klsu" class="btn btn-primary">Ubah</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </form>
    <!-- /.modal -->

    <!-- modal hapus kelas umum -->
    <form class="form-horizontal">
        <div class="modal fade" id="hapusKelasModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <input type="text" id="del_id" hidden>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Anda yakin akan menghapus kelas <span id="del_nama_matkul"></span></p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="button" id="btn_hapus_klsu" class="btn btn-danger">Saya Yakin</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
    <!-- /.modal -->
    <!-- tutup modal hapus kelas umum -->



    <!-- Modal Info -->
    <div class="modal fade modal-gabung" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Gabung ke Kelas :</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>PIN :</label>
                        <input type="text" class="colorpicker-default form-control colorpicker-element"
                            data-colorpicker-id="1" data-original-title="" title="" id="pin" placeholder="e.g ABCDE">
                    </div>
                    <button type="button" class="btn btn-primary" onClick="GabungKelas()">Buat</button>
                    <button type="button" class="btn btn-secondary">Tutup</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<script type="text/javascript">

function GabungKelas(){
    
    var hr = new XMLHttpRequest();
    var url = "<?= base_url('user/gabungkelas')?>";
    var pin = document.getElementById("pin").value;
    var vars = "pin="+pin;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
                swal({
                    title: "Gabung Kelas!",
                    text: return_data,
                    icon: "success",
                }).then(function() {
                    window.location = "<?= base_url('user/ruangkelas')?>";
                });
        }
    }
    hr.send(vars); 


}


$(document).ready(function() {
    listKelass(); //pemanggilan fungsi tampil barang.

    // $('#listKelasTable').dataTable();

    //fungsi kelas umum
    function listKelass() {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('kelas_umum/ListKelas'); ?>',
            async: true,
            dataType: 'json',
            success: function(data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    const nama = data[i].user_update;
                    const ubah = nama.replace(/\s/g, '-'); 
                    html += '<tr>' +
                        '<td><a  href="informasi_kelas' + '?id=' + data[i].id +
                        '"><strong style="color: blue;">' + data[i].nama_matkul +
                        '</strong></a><br><p style="font: 10px;">' + data[i].kode_kelas +
                        '</p></td>' +
                        '<td><i class="fas fa-user-graduate"></i><a class="has-text-grey" href="profile_umum' +
                        '?nama_user=' + ubah + '">' + data[i].user_update +
                        '</a></td>' +
                        '<td>'+data[i].jml+' Peserta</td>' +
                        '<td>'+data[i].tanggal_update+'</td>' +
                        '<td><div class="dropdown"><button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="icon"><i class="mdi mdi-dots-vertical mdi-18px"></i></span></button><div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                        '<a class="dropdown-item item_edit_klsu" href="javascript;0" data="' + data[
                            i].id + '">Ubah Kelas</a>' +
                        '<a class="dropdown-item" href="#">Duplikat Semua Sesi dan materi</a>' +
                        '<a class="dropdown-item item_hapus_klsu" href="javascript;0" data="' +
                        data[i].id + '" style="color : red;" href="#">Hapus</a>' +
                        '</div></div></td>' +
                        '</tr>';

                }
                $('#listKelas2').html(html);
            }

        });
    }

    //Simpan kelas umum
    $('#btn_simpan_klsu').on('click', function() {
        var nama = $('#nama').val();
        var id_kelas = $('#id_kelas').val();
        var deskripsi = $('#deskripsi').val();
        var customRadioInline1 = $('#customRadioInline1').val();
        var customRadioInline2 = $('#customRadioInline2').val();
        var keterangan = $('#keterangan').val();
        if (customRadioInline1 == null) {
            var type = customRadioInline2;
        } else {
            var type = customRadioInline1;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kelas_umum/SaveKelas'); ?>",
            async: true,
            dataType: 'json',
            data: {
                nama: nama,
                id_kelas: id_kelas,
                deskripsi: deskripsi,
                type: type,
                keterangan: keterangan
            },
            success: function(data) {
                $('#nama').val("");
                $('#id_kelas').val("");
                $('#deskripsi').val("");
                $('#customRadioInline1').val("");
                $('#customRadioInline2').val("");
                $('#keterangan').val("");
                $('#addKelasModal').modal('hide');
                listKelass();
            }
        });
        return false;
    });



    //Update Kelas Umum
    $('#btn_ubah_klsu').on('click', function() {
        var nama = $('#get_nama').val();
        var id = $('#get_id').val();
        var id_kelas = $('#get_id_kelas').val();
        var deskripsi = $('#get_deskripsi').val();
        var get_customRadioInline1 = $('#get_customRadioInline1').val();
        var get_customRadioInline2 = $('#get_customRadioInlineq').val();
        if (get_customRadioInline1 == null) {
            var type = get_customRadioInline2;
        } else {
            var type = get_customRadioInline1;
        }
        var keterangan = $('#get_keterangan').val();
        //console.log($nama);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kelas_umum/UbahKelas'); ?>",
            async: true,
            dataType: 'json',
            data: {
                id: id,
                nama: nama,
                id_kelas: id_kelas,
                deskripsi: deskripsi,
                type: type,
                keterangan: keterangan
            },
            success: function(data) {
                $('#get_nama').val("");
                $('#get_id').val("");
                $('#get_id_kelas').val("");
                $('#get_deskripsi').val("");
                $('#get_customRadioInline1').val("");
                $('#get_customRadioInline2').val("");
                $('#get_keterangan').val("");
                $('#editKelasModal').modal('hide');
                listKelass();
            }
        });
        return false;
    });



    //GET UPDATE kelas umum
    $('#listKelas2').on('click', '.item_edit_klsu', function() {
        var id = $(this).attr('data');
        //console.log(id);
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('kelas_umum/GetKelas'); ?>",
            dataType: "JSON",
            data: {
                id: id
            },
            success: function(data) {
                //console.log(data);
                $('#editKelasModal').modal('show');
                $('#get_id_kelas').val(data.kode_kelas);
                $('#get_nama').val(data.nama_matkul);
                $('#get_id').val(data.id);
                $('#nama_kelas_umum').html(data.nama_matkul);
                $('#get_deskripsi').html(data.deskripsi);
                $('#get_keterangan').html(data.keterangan);
                var type2 = data.type;
                if (type2 == 'Publik') {
                    var type = "get_customRadioInline1";
                } else {
                    var type = "get_customRadioInline2";
                }
                document.getElementById(type).checked = true;
            }
        });
        return false;
    });


    //GET Hapus kelas umum
    $('#listKelas2').on('click', '.item_hapus_klsu', function() {
        var id = $(this).attr('data');
        //console.log(id);
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('kelas_umum/GetKelas'); ?>",
            dataType: "JSON",
            data: {
                id: id
            },
            success: function(data) {
                //console.log(data);
                $('#hapusKelasModal').modal('show');
                $('#del_nama_matkul').html(data.nama_matkul);
                $('#del_id').val(data.id);
                // console.log(data.nama_matkul);
            }
        });
        return false;
    });

    //Hapus Kelas Umum
    $('#btn_hapus_klsu').on('click', function() {
        var id = $('#del_id').val();
        //console.log($nama);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kelas_umum/HapusKelas'); ?>",
            async: true,
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                $('#del_id').val("");
                $('#hapusKelasModal').modal('hide');
                listKelass();
            }
        });
        return false;
    });





});
</script>


<!-- tutup script buat kelas -->