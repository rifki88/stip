   <!-- ============================================================== -->
   <!-- Start right Content here -->
   <!-- ============================================================== -->
   <div class="main-content">

       <div class="page-content">
           <div class="row" style="height: 250px; background-color: #81d4fa;">
               <div class="container-fluid">
                   <div class="row">
                       <div class="col-sm-3">
                           <br>
                           <br>
                           <br>
                           <figure class="image is-16by9 img-bg mr-5">
                               <div class="overlay animated fadeIn"
                                   style="border: 2px dashed rgb(255, 255, 255); box-sizing: border-box; border-radius: 4px; cursor: pointer;">
                                   <div class="columns is-vcentered" style="margin: 0px; height: 100%;">
                                       <div class="column has-text-centered">
                                           <figure class="image is-48x48" style="margin: 0px auto;">
                                               <svg width="48" height="48" viewBox="0 0 48 48" fill="none"
                                                   xmlns="http://www.w3.org/2000/svg" style="margin: 10% 0 0 40%;">
                                                   <g opacity="0.8">
                                                       <path
                                                           d="M13.457 36.0838C10.5228 36.0838 7.90957 34.2052 6.96084 31.406L6.89772 31.1938C6.67481 30.4512 6.57879 29.8301 6.57879 29.2057V16.7062L2.13457 31.557C1.56251 33.7405 2.86576 36.0066 5.05262 36.6089L33.4039 44.2022C33.7564 44.2962 34.111 44.3371 34.4574 44.3371C36.2837 44.3371 37.9529 43.1272 38.4175 41.3392L40.0679 36.0865H13.457V36.0838Z"
                                                           fill="white"></path>
                                                       <path
                                                           d="M18.4979 16.8325C20.5196 16.8325 22.164 15.1881 22.164 13.1665C22.164 11.1448 20.5196 9.50043 18.4979 9.50043C16.4762 9.50043 14.8319 11.1448 14.8319 13.1665C14.8319 15.1881 16.4762 16.8325 18.4979 16.8325Z"
                                                           fill="#FAFAFA"></path>
                                                       <path
                                                           d="M41.4154 4H13.9135C11.3862 4 9.32892 6.05728 9.32892 8.58456V28.7518C9.32892 31.279 11.3862 33.3363 13.9135 33.3363H41.4154C43.9427 33.3363 46 31.279 46 28.7518V8.58456C46 6.05459 43.9427 4 41.4154 4ZM13.9135 7.66603H41.4154C41.9217 7.66603 42.3313 8.07561 42.3313 8.58187V21.5983L36.5395 14.841C35.9258 14.1205 35.0348 13.7358 34.0807 13.7136C33.1319 13.719 32.2383 14.14 31.6299 14.8719L24.8236 23.0452L22.6065 20.8282C21.353 19.5746 19.3118 19.5746 18.0582 20.8282L12.9976 25.8888V8.58187C12.9976 8.07561 13.4072 7.66603 13.9135 7.66603Z"
                                                           fill="white"></path>
                                                   </g>
                                               </svg>
                                           </figure>
                                           <p style="text-align:center; padding: 10px; color: #ffffff;">Tambahkan Gambar
                                               untuk Thumbnail Kelas</p>
                                       </div>
                                   </div>
                               </div>
                           </figure>
                       </div>
                       <div class="col-sm-6" style="color: #ffffff;">
                           <br><br>
                           <div class="row">
                               <h3>Kelas Manajemen Informatika <?= $ambil[0]['nama_matkul']?></h3>
                               <!-- <h5></h5> -->
                           </div>
                           <br><br><br>
                           <div class="row" style="vertical-align: bottom;">
                               <div class="col-md-4">
                                   <div class="row">
                                       Kode Kelas
                                   </div>
                                   <div class="row">
                                       <?= $ambil[0]['id']; ?>
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="row">
                                       Jumlah Anggota
                                   </div>
                                   <div class="row">
                                       <?= $memberkelas; ?> Anggota
                                   </div>
                               </div>
                               <div class="col-md-4">
                                   <div class="row">
                                       Pengelola
                                   </div>
                                   <div class="row">
                                       <?= $ambil[0]['user_update']?>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="col-sm-3">
                           <div class="row">
                               <div class="col-md-12">
                                   <a href="#" style="float: right; padding-top: 10%; color: #ffffff;">Ganti Cover</a>
                               </div>
                           </div>
                           <br><br><br><br><br>
                           <div class="row">
                               <div class="col-md-12">
                                   <a href="#"
                                       style="float: right; padding-top: 10%; color: #ffffff;"><span>Bagikan</span><br>Tampilkan
                                       PIN/LINK&nbsp;&nbsp;<i class="ti ti-share"></i> </a>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

           <!-- start page title -->
           <div class="row">
               <div class="container-fluid">
                   <div class="row">

                       <div class="col-sm-2">
                           <div class="page-title-box">
                               <div class="row" style="padding: 10px;">
                                   <div class="col-md-12">
                                       <a href="#"><b><span class="icon"><svg width="16" height="10" viewBox="0 0 16 10"
                                                       fill="none" xmlns="http://www.w3.org/2000/svg"
                                                       style="width: 20px;">
                                                       <path
                                                           d="M5.08366 8.75L1.33366 5M1.33366 5L5.08366 1.25M1.33366 5L14.667 5"
                                                           stroke="#039be5" stroke-width="1.8" stroke-linecap="round"
                                                           stroke-linejoin="round"></path>
                                                   </svg></span>&nbsp;&nbsp;Kembali</b></a>
                                   </div>
                               </div>
                           </div>
                       </div>
                     
                           <div class="col-sm-9">
                           <form method="POST" action="<?= base_url('user/save_quiz/'.$this->uri->segment(3))?>" enctype="multipart/form-data">
                               <div class="page-title-box">
                                   <div class="card text-black">
                                       <div class="card-body">

                                           <div class="row" style="border-bottom: 1px solid gray;">
                                               <div class="col-md-10">
                                                   <p>Info Bahan Pembelajaran</p>
                                               </div>
                                               <div class="col-md-2">
                                                   <button type="submit" style="float: right;"><i
                                                           class="ti ti-save"></i>&nbsp;&nbsp;Simpan</button>
                                               </div>
                                           </div>
                                           <br>
                                           <div class="form-group">
                                               <label class="font-size-20">Judul</label>
                                               <input type="text" class="form-control"
                                                   placeholder="Masukkan judul quiz" name="judul_quiz">
                                           </div>
                                           <br>
                                           <div class="form-group">
                                               <label class="font-size-16">Kapan kuis akan dibagikan?</label><br>
                                               <button type="button" class="btn btn-info waves-effect waves-light"> <i
                                                       class="ti ti-check"></i> Sekarang</button>
                                               <button type="button" class="btn btn-secondary waves-effect waves-light"
                                                   onclick="hideTime7()"> <i class="ti ti-alarm-clock"></i>
                                                   Nanti</button>
                                           </div>
                                           <div class="form-group time7" id="time7" style="display: none;">
                                               <label>Pilih tanggal dan waktu</label><br>
                                               <input class="form-control" type="datetime-local"
                                                   value="2011-08-19T13:45:00" name="tanggal_dibagikan" id="example-datetime-local-input">
                                           </div>
                                           <br>
                                           <div class="form-group">
                                               <label class="d-block mb-3 font-size-16">Indikator Penilaian :</label>
                                               <div class="custom-control custom-radio custom-control-inline">
                                                   <input type="radio" id="radioSurvei1"
                                                       name="indikator_penilaian" class="custom-control-input"  value="manual">
                                                   <label class="custom-control-label" for="radioSurvei1">Bobot per
                                                       soal: (Nilai bobot per soal ditentukan secara manual)</label>
                                               </div>
                                               <br>
                                               <div class="custom-control custom-radio custom-control-inline">
                                                   <input type="radio" id="radioSurvei2"
                                                       name="indikator_penilaian" class="custom-control-input" value="otomatis">
                                                   <label class="custom-control-label" for="radioSurvei2" >Bobot
                                                       otomatis: (Sistem otomatis membagi nilai bobot per soal)</label>
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <label>Tambahkan batas nilai</label><br>
                                               <input type="checkbox" id="switch4" switch="bool" onclick="mySerah1()" />
                                               <label for="switch4" data-on-label="Ya" data-off-label="Tidak"></label>
                                           </div>
                                           <div class="form-group batas" id="batas" style="display: none;">
                                               <label>Batas Nilai Minimal Lulus (Max: 100)</label>
                                               <input type="text" class="form-control"
                                                   placeholder="Masukkan batas nilai" name="batas_nilai" style="width: 30%;">
                                           </div>
                                           <br>
                                           <div class="form-group">
                                               <label>Durasi Pengerjaan <i> (Dalam Menit) </i></label>
                                               <input type="text" class="form-control" name="batas_menit"
                                                   placeholder="Masukkan durasi pengerjaan" style="width: 30%;">
                                           </div>
                                           <div class="form-group">
                                               <label>Batas Waktu</label><br>
                                               <input type="checkbox" id="switch5" switch="bool" onclick="mySerah2()" />
                                               <label for="switch5" data-on-label="Ya" data-off-label="Tidak"></label>
                                           </div>
                                           <div class="form-group batas1" id="batas1" style="display: none;">
                                               <label>Pilih tanggal dan waktu</label><br>
                                               <input class="form-control" type="datetime-local" name="batas_waktu"
                                                   value="2011-08-19T13:45:00" id="example-datetime-local-input"
                                                   style="width: 30%;">
                                           </div>
                                           <br>
                                           <div class="form-group">
                                               <label>Tampilkan laporan kepada peserta setelah quiz selesai?</label><br>
                                               <input type="checkbox" id="laporan" name="laporan" switch="bool" />
                                               <label for="laporan" data-on-label="Ya" data-off-label="Tidak"></label>
                                           </div>

                                       </div>

                                   </div>
                               </div>

                               <div class="card text-black">
                                   <div class="card-body">
                                       <div class="row" style="border-bottom: 1px solid gray;">
                                           <div class="col-md-10 font-size-16">
                                               <p>Pertanyaan Quiz</p>
                                           </div>
                                       </div>
                                       <br>
                                       <div class="row">
                                           <div class="col-md-1">
                                               <b>1</b>
                                           </div>
                                           <div class="col-md-9">
                                               <b>Contoh pertanyaan pertama</b>
                                           </div>
                                           <div class="col-md-2">
                                               <button type="button"
                                                   class="btn btn-primary waves-effect waves-light mr-2"><i
                                                       class="ti ti-pencil"></i></button>
                                               <button type="button" class="btn btn-danger waves-effect waves-light"><i
                                                       class="ti ti-trash"></i></button>
                                           </div>
                                       </div>
                                       <br>
                                       <div class="row">
                                           <div class="col-md-1">
                                               <b>2</b>
                                           </div>
                                           <div class="col-md-9">
                                               <b>Contoh pertanyaan kedua</b>
                                           </div>
                                           <div class="col-md-2">
                                               <button type="button"
                                                   class="btn btn-primary waves-effect waves-light mr-2"><i
                                                       class="ti ti-pencil"></i></button>
                                               <button type="button" class="btn btn-danger waves-effect waves-light"><i
                                                       class="ti ti-trash"></i></button>
                                           </div>
                                       </div>
                                       <br>
                                       <div class="row">
                                           <div class="col-md-6">
                                               <button type="button"
                                                   class="btn btn-block btn-outline-info waves-effect waves-light"
                                                   onclick="hideQues1()">Tambah Pertanyaan</button>
                                           </div>
                                           <div class="col-md-6">
                                               <button type="button"
                                                   class="btn btn-block btn-outline-info waves-effect waves-light">Import
                                                   Pertanyaan</button>
                                           </div>
                                       </div>
                                       <br>
                                       <div class="question-container" id="quesbox" style="display: none;">
                                           <br>
                                           <div class="row">
                                               <div class="col-md-2">
                                                   <b> Pertanyaan </b>
                                               </div>
                                               <div class="col-md-10">
                                                   <textarea name="soal" class="summernote" name="" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <br>
                                           <div class="row">
                                               <div class="col-md-2">
                                                   <b> Gambar Tautan </b>
                                               </div>
                                               <div class="col-md-10">
                                                   <input type="file" class="filestyle" name="foto_soal" required>
                                               </div>
                                           </div>
                                           <br>
                                           <div class="row">
                                               <div class="col-md-2">
                                                   <b> Jawaban </b>
                                               </div>
                                               <div class="col-md-1">
                                                   <b> A </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="pil_a" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-2">

                                               </div>
                                               <div class="col-md-1">
                                                   <b> B </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="pil_b" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-2">

                                               </div>
                                               <div class="col-md-1">
                                                   <b> C </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="pil_c" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-2">

                                               </div>
                                               <div class="col-md-1">
                                                   <b> D </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="pil_d" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-2">

                                               </div>
                                               <div class="col-md-1">
                                                   <b> E </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="pil_e" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-2">

                                               </div>
                                               <div class="col-md-1">
                                                   <b> Jawaban Benar </b>
                                               </div>
                                               <div class="col-md-9">
                                                   <textarea name="jawaban" class="summernote" id="summernote"></textarea>
                                               </div>
                                           </div>
                                           <br>
                                           <!-- <div class="row">
                                               <div class="col-md-6">
                                                   <button type="button"
                                                       class="btn btn-block btn-outline-danger waves-effect waves-light"
                                                       onclick="hideQues1()">Batal</button>
                                               </div>
                                               <div class="col-md-6">
                                                   <button type="button"
                                                       class="btn btn-block btn-outline-info waves-effect waves-light">Simpan
                                                       Pertanyaan</button>
                                               </div>
                                           </div> -->
                                       </div>
                                   </div>

                               </div>

                               <div class="card text-black">
                                   <div class="card-body">
                                       <div class="row" style="border-bottom: 1px solid gray;">
                                           <div class="col-md-10 font-size-16">
                                               <p>Catatan</p>
                                           </div>
                                       </div>
                                       <br>
                                       <div class="form-group">
                                           <label>Catatan</label>
                                           <textarea id="textarea" name="catatan" class="form-control summernote" id="summernote"
                                               maxlength="225" rows="1" style="height: 100px;"></textarea>
                                       </div>
                                   </div>
                               </div>
                       </form>
                   </div>

               </div>
           </div>
       </div>
       <!-- end page title -->


   </div>
   <!-- End Page-content -->



   <footer class="footer">
       <div class="container-fluid">
           <div class="row">
               <div class="col-12">
                   © <script>
                   document.write(new Date().getFullYear())
                   </script> STIP JAKARTA
               </div>
           </div>
       </div>
   </footer>

   </div>

   <script>
function hideTime7() {
    var x = document.getElementById("time7");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function hideQues1() {
    var x = document.getElementById("quesbox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function mySerah1() {
    var checkBox = document.getElementById("switch4");
    var text = document.getElementById("batas");
    if (checkBox.checked == true) {
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}

function mySerah2() {
    var checkBox = document.getElementById("switch5");
    var text = document.getElementById("batas1");
    if (checkBox.checked == true) {
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}
   </script>