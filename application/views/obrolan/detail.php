   <!-- ============================================================== -->
   <!-- Start right Content here -->
   <!-- ============================================================== -->
   <div class="main-content">

       <div class="page-content">
           <div class="container-fluid">

               <br>
               <!-- Menu tab -->
               <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                   <li class="nav-item">
                       <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                           <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                           <span class="d-none d-sm-block"><i class="ti-user mr-2"></i>Kontak</span>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                           <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                           <span class="d-none d-sm-block"><i class="ti-comments mr-2"></i>Pesan</span>
                       </a>
                   </li>
               </ul>
               <!-- selesai menu tab -->

               <!-- Isi Tab -->
               <div class="tab-content">
                   <div class="tab-pane active p-3" id="home" role="tabpanel">
                       <div class="row">
                           <div class="row col-md-3"
                               style="border-right: 1px solid #e6e6e6; margin-top: 0px; padding-top: 0px;">
                               <h4>Kontak</h4>
                               <div class="form-group">
                                   <input class="form-control" type="search" id="example-search-input"
                                       placeholder="Cari ...">
                               </div>
                               <ul class="list-group">
                                   <?php 
                                        $jumlah = count($data); 
                                        for($i=0;$i<$jumlah;$i++){
                                        ?>
                                   <li class="list-group-item"><a href="<?= base_url('user/obrolandetail/'.$data[$i]['id_user'])?>"><?= $data[$i]['full_name']?> / <?= $data[$i]['phone']?> </a></li>
                                   
                                   <?php } ?>
                               </ul>
                           </div>
                           <div class="col-md-9">
                                    <h3 align="center">Detail User : <?= $detail[0]['full_name'];?></h3>
                                    <hr>
                                   <div class="form-row">
                                       <div class="form-group col-md-6 text-center">
                                       <label>Email : </label>
                                           <?= $detail[0]['email'];?>
                                       </div>
                                       <div class="form-group col-md-6 text-center">
                                       <label>Fullname : </label>
                                       <?= $detail[0]['full_name'];?>
                                       </div>
                                   </div>

                                   <div class="form-row">
                                       <div class="form-group col-md-6 text-center">
                                       <label>Phone : </label>
                                           <?= $detail[0]['phone'];?>
                                       </div>
                                       <div class="form-group col-md-6 text-center">
                                       <label>Gender : </label>
                                       <?= $detail[0]['gender'];?>
                                       </div>
                                   </div>
                                   <div class="form-row">
                                       <div class="form-group col-md-6 text-center">
                                       <label>Access : </label>
                                           <?= $detail[0]['access'];?>
                                       </div>
                                       <div class="form-group col-md-6 text-center">
                                       <label>Token : </label>
                                       <?= $detail[0]['session_id'];?>
                                       </div>
                                   </div>
                           </div>
                       </div>
                   </div>
                   <div class="tab-pane p-3" id="profile" role="tabpanel">
                       <div class="row">
                           <div class="row col-md-3"
                               style="border-right: 1px solid #e6e6e6; margin-top: 0px; padding-top: 0px;">
                               <div class="row col-md-12">
                                   <h4>Pesan</h4>
                               </div>
                               <div class="form-group">
                                   <input class="form-control" type="search" id="example-search-input"
                                       placeholder="Cari ...">
                               </div>
                               <div class="row col-md-12">
                                 
                               <ul class="list-group">
                                   <?php 
                                        $count = count($data); 
                                        for($j=0;$j<$count;$j++){
                                        ?>
                                   <li class="list-group-item"> <a href="#"><?= $data[$j]['email']?> </a> </li>
                                   
                                   <?php } ?>
                               </ul>


                               </div>
                           </div>
                           <div class="col-md-9">
                           <h3 align="center">Form Obrolan</h3>
                                <hr>
                                   <div class="form-row">
                                       <div class="form-group col-md-6">
                                       <label>Dari : </label>
                                        <input type="text" class="form-control" name="dari" placeholder="dari">
                                       </div>
                                       <div class="form-group col-md-6">
                                       <label>Tujuan : </label>
                                       <input type="text" class="form-control" name="tujuan" placeholder="tujuan">
                                       </div>
                                   </div>

                                   <div class="form-row">
                                       <div class="form-group col-md-12">
                                       <label>Isi : </label>
                                       <textarea class="form-control" name="isi">

                                        </textarea>
                                       </div>
                                   </div>

                                   <button type="submit" class="btn btn-primary">Send</button>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- Selesai isi tab -->

           </div> <!-- container-fluid -->
       </div>
       <!-- End Page-content -->


       <footer class="footer">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-12">
                       © <script>
                       document.write(new Date().getFullYear())
                       </script> STIP JAKARTA
                   </div>
               </div>
           </div>
       </footer>

   </div>
   <!-- end main content-->

   </div>
   <!-- END layout-wrapper -->