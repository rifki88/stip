<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Semua Jadwal</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/add_jadwal');?>" class="btn btn-primary" >Add</a>      
                                    </p>
                                        <table style="width: 100%; font-size: 14px;" class="table table-striped border"
                                            >
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Keterangan</th>
                                                    <th>Tanggal</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px">
                                             <?php
                                                $jumlah = count($data);
                                                for($i=0;$i<$jumlah;$i++){
                                                ?>
                                                <tr>
                                                    <td><?= $data[$i]['id']?></td>
                                                    <td><?= $data[$i]['nama_jadwal']?></td>
                                                    <td><?= $data[$i]['keterangan']?></td>
                                                    <td><?= $data[$i]['tanggal']?></td>
                                                    <td>
                                                        <a href="<?= base_url('user/delete_jadwal/'.$data[$i]['id']) ?>" onClick="return confirm('Yakin mau hapus jadwal ini ??')" class="btn btn-danger">Delete </a>
                                                </td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->