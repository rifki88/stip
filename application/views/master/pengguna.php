<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Pengguna</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <table style="width: 100%; font-size: 14px;" class="table table-striped border"
                                            >
                                            <thead>
                                                <tr>
                                                    <th>ID User</th>
                                                    <th>Email</th>
                                                    <th>Fullname</th>
                                                    <th>Phone</th>
                                                    <th>Access</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px">
                                             <?php
                                                $jumlah = count($data);
                                                for($i=0;$i<$jumlah;$i++){
                                                ?>
                                                <tr>
                                                    <td><?= $data[$i]['id_user']?></td>
                                                    <td><?= $data[$i]['email']?></td>
                                                    <td><?= $data[$i]['full_name']?></td>
                                                    <td><?= $data[$i]['phone']?></td>
                                                    <td><?= $data[$i]['access']?></td>
                                                    <td>
                                                        <a href="<?= base_url('user/edit_pengguna/'.$data[$i]['id_user'])?>" class="btn btn-warning">Ubah</a>
                                                        <a onclick="return confirm('yakin mau hapus ??')" href="<?= base_url('user/delete_user/'.$data[$i]['id_user'])?>" class="btn btn-danger">Delete</a></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->