<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Edit User</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/pengguna');?>" class="btn btn-primary" >Back </a>      
                                    </p>
                                    <div class="col-md-12">
                                                              

                                    <form method="POST" action="<?= base_url('user/proses_editpengguna')?>">
                                    <input type="hidden" name="id_user" value="<?= $data[0]['id_user']?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Email</label>
                                                <input type="text" class="form-control" name="email" value="<?= $data[0]['email']?>" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Password</label>
                                                <input type="text" class="form-control" name="password">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Fullname</label>
                                                <input type="text" class="form-control" name="full_name" value="<?= $data[0]['full_name']?>">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Phone</label>
                                                <input type="text" class="form-control" name="phone" value="<?= $data[0]['phone']?>">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Gender</label>
                                                <input type="radio"  name="gender" value="L" checked>L
                                                <input type="radio"   name="gender" value="P">P
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Access</label>
                                                <input type="text" class="form-control" name="access" value="<?= $data[0]['access']?>">
                                                </div>
                                            </div>
                                           
                                          
                                           
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </form>



                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->