<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Jadwal</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/jadwal');?>" class="btn btn-primary" >Back </a>      
                                    </p>
                                    <div class="col-md-12">
                                                              

                                        <form method="POST" action="<?= base_url('user/insert_jadwal')?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="namaJadwal">Nama Jadwal</label>
                                                <input type="text" class="form-control" id="inputEmail4" name="nama_jadwal" placeholder="Nama Jadwal">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="keteranganJadwal">keterangan</label>
                                                <input type="text" class="form-control" id="inputPassword4" name="keterangan" placeholder="Keterangan">
                                                </div>
                                            </div>
                                          
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                <label for="namaJadwal">Tanggal</label>
                                                <input type="date" class="form-control" id="inputDate" name="tanggal" placeholder="Tanggal">
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </form>



                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->