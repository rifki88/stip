<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Add Sesi Akademik</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/ruangkelas');?>" class="btn btn-primary" >Back </a>      
                                    </p>
                                    <div class="col-md-12">
                                                              

                                    <form method="POST" action="<?= base_url('user/insert_sesi_akademik/'.$this->uri->segment(3))?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Tujuan</label>
                                                <input type="text" class="form-control" id="inputEmail4" name="tujuan" placeholder="Tujuan">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Nama Sesi</label>
                                                <input type="text" class="form-control" id="inputPassword4" name="nama_sesi" placeholder="Nama Sesi">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Kelas</label>
                                                <select class="form-control" id="inputEmail4" name="id_kelas">
                                                    <option value="">Pilih Kelas</option>
                                                   <?php
                                                        $jml = count($kelas);
                                                        for($i=0;$i<$jml;$i++){
                                                            echo "<option value=".$kelas[$i]['kode_kelas'].">".$kelas[$i]['kode_kelas']."</option>";
                                                        }

                                                    ?>
                                                </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Tanggal Sesi</label>
                                                <input type="date" class="form-control" id="inputPassword4" name="tanggal" placeholder="Tanggal">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Waktu Mulai</label>
                                                <input type="time" class="form-control" id="inputEmail4" name="waktu_mulai" placeholder="Waktu Mulai">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Waktu Akhir</label>
                                                <input type="time" class="form-control" id="inputPassword4" name="waktu_akhir" placeholder="Waktu Akhir">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                <label for="inputEmail4"></label>
                                                <input type="hidden" class="form-control" id="inputEmail4" name="type" value="2">
                                                </div>
                                               
                                            </div>
                                           
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </form>



                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->