<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Sesi</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/add_sesi');?>" class="btn btn-primary" >Add</a>      
                                    </p>
                                    <div class="col-md-12">
                                        <table style="width: 100%; font-size: 14px;" class="table table-striped border"
                                            >
                                            <thead>
                                                <tr>
                                                    <th>ID Sesi</th>
                                                    <th>Tujuan</th>
                                                    <th>Nama Sesi</th>
                                                    <th>Kelas</th>
                                                    <th>Waktu Mulai</th>
                                                    <th>Waktu Selesai</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-size: 12px">
                                            
                                            <?php
                                                $jumlah = count($data);
                                                for($i=0;$i<$jumlah;$i++){
                                                ?>
                                                <tr>
                                                    <td><?= $data[$i]['id']?></td>
                                                    <td><?= $data[$i]['tujuan']?></td>
                                                    <td><?= $data[$i]['nama_sesi']?></td>
                                                    <td><?= $data[$i]['id_kelas']?></td>
                                                    <td><?= $data[$i]['waktu_mulai']?></td>
                                                    <td><?= $data[$i]['waktu_akhir']?></td>
                                                    <td><?= $data[$i]['status']?></td>
                                                    <td><a onClick="return confirm('Yakin Mau Hapus Sesi ini ?')"  href="<?= base_url('user/delete_sesi/'.$data[$i]['id']) ?>" class="btn btn-danger">Delete </a></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->