<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <br>
            <div class="row">
                <h4>Edit Kelas Umum</h4>
            </div>
            <br>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12 card">
                        <div class="tab-pane active p-3">
                            <p class="mb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align:right">
                                        <a href="<?= base_url('user/kelasumum');?>" class="btn btn-primary" >Back </a>      
                                    </p>
                                    <div class="col-md-12">
                                                              

                                    <form method="POST" action="<?= base_url('user/update_kelasumum')?>" enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="<?= $data[0]['id']?>">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Kode Kelas</label>
                                                <input type="text" class="form-control" name="kode_kelas" value="<?= $data[0]['kode_kelas']?>" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Nama Mata Kuliah</label>
                                                <input type="text" class="form-control" name="nama_matkul" value="<?= $data[0]['nama_matkul']?>">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Type Kelas</label>
                                                <br>
                                                <input type="radio"  name="type" value="Publik" checked>Publik
                                                <input type="radio"   name="type" value="Pribadi">Pribadi
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Deskripsi</label>
                                                <input type="text" class="form-control" name="deskripsi" value="<?= $data[0]['deskripsi']?>">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Keterangan</label>
                                                <input type="text" class="form-control" name="keterangan" value="<?= $data[0]['keterangan']?>">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputPassword4">Thumbnail</label>
                                                <input type="file" class="form-control" name="thumbnail">
                                                </div>
                                            </div>
                                           
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Cover</label>
                                                <input type="file" class="form-control" name="cover">
                                                </div>
                                                <div class="form-group col-md-6">
                                                <label for="inputEmail4">Status</label>
                                                <input type="text" class="form-control" name="status" value="<?= $data[0]['status']?>" disabled>
                                                </div>
                                               
                                            </div>
                                           
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                            </form>



                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    
                </div>
                <!-- Nav tabs -->
            </div>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <script>
                    document.write(new Date().getFullYear())
                    </script> STIP JAKARTA
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- script data buat kelas umum-->
<script src="<?= base_url();?>/assets/libs/jquery/jquery.min.js"></script>

<!-- tutup script buat kelas -->