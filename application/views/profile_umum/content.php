  <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <img class="rounded-circle" alt="200x200" src="assets/images/users/user-4.jpg" data-holder-rendered="true">
                            </div>
                            <div class="col-md-4">
                                <h4 style="color: #212121;"><?= $profile[0]['full_name'];?></h4>
                                <span style="color: #9e9e9e;">Tambahkan ringkasan tentang Anda</span><br><br>
                                <a href="#"><i class="ti-link mr-2"></i> -</a>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a href="#"><i class="ti-pencil mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="card text-white" style="padding: 20px; background-color: #64b5f6;">
                                            <div class="row">
                                                <div class="col-md-3" style="padding-top:5px;">
                                                    <i class="ti-write mr-2" style="font-size: 24px;"></i>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="col-md-12">Memposting</div>
                                                        <div class="col-md-12"><b><?= $total; ?> Kali</b></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="card text-white" style="padding: 20px; background-color: #64b5f6;">
                                            <div class="row">
                                                <div class="col-md-3" style="padding-top:5px;">
                                                    <i class="ti-bag mr-2" style="font-size: 24px;"></i>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="col-md-12">Menugaskan</div>
                                                        <div class="col-md-12"><b>0 Kali</b></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="border-bottom: 1px solid #e6e6e6; width: 300%;">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs nav-tabs-custom" role="tablist" style="border-bottom: 1px solid #e6e6e6;">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                            <span class="d-none d-sm-block">Postingan</span> 
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                            <span class="d-none d-sm-block">Kelas</span> 
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">
                                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                            <span class="d-none d-sm-block">Tentang</span>   
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                            <!-- Nav tabs -->
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="tab-content">
                                    <div class="tab-pane active p-3" id="home1" role="tabpanel">
                                        <div class="row">
                                        <div class="card text-black">
                                            <?php
                                                $jumlah =count($materi);
                                                for($i=0;$i<$jumlah;$i++){
                                                ?>
                                            <div class="card-body">
                                                <div class="media mb-4">
                                                    <img class="d-flex mr-3 rounded-circle"
                                                        src="<?= base_url();?>/assets/images/users/user-6.jpg"
                                                        alt="Generic placeholder image" height="64">
                                                    <div class="media-body">
                                                        <h6 class="mt-2"><?= $materi[$i]['user_update']?> <span>memposting
                                                                di <a href="#">Grup Kelas Umum</a></span></h6>
                                                        <small>3 jam yang lalu</small>
                                                    </div>
                                                </div>
                                                <p> <?= $materi[$i]['keterangan']?> </p>
                                                <?php
                                                if(empty($materi[$i]['thumbnail'])){
                                                ?>
                                                <img src="<?= base_url();?>/assets/images/small/img-2.jpg" class="img-fluid"
                                                    alt="Responsive image">
                                                <?php }else{ ?>
                                                <img src="<?= base_url();?>/assets/upload/thumbnail/<?= $materi[$i]['thumbnail'];?>"
                                                    class="img-fluid" alt="Responsive image" style="border:1px solid #000;">
                                                <?php  } ?>
                                                <div class="mt-3 mb-2" style="border-bottom: 2px solid #e0e0e0;"></div>
                                                <!--i class="ti-heart"></i>&nbsp;<a href="#" style="color: #6d6d6d;">Suka</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="ti-comments"></i>&nbsp;<a href="#" style="color: #6d6d6d;">Komentar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="ti-share"></i-->
                                                <!--a href="#" style="color: #6d6d6d;">Bagikan</a-->

                                                <?php
                                                for($j=0;$j<count($materi[$i]['komentar']);$j++){
                                                ?>
                                                <div class="card text-black bg-secondary mt-1">
                                                    <div class="card-body">
                                                        <div class="media">
                                                            <img class="d-flex mr-3 rounded-circle"
                                                                src="<?= base_url();?>/assets/images/users/user-6.jpg"
                                                                alt="Generic placeholder image" height="32">
                                                            <div class="media-body">
                                                                <h6 class="">
                                                                    <?= $materi[$i]['komentar'][$j]['user_update']?></h6>
                                                                <p><?= $materi[$i]['komentar'][$j]['komentar']?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                               
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <img class="d-flex mr-3 rounded-circle"
                                                                src="<?= base_url();?>/assets/images/users/user-6.jpg"
                                                                alt="Generic placeholder image" height="32">
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane p-3" id="profile1" role="tabpanel">
                                        <div class="card text-gray bg-secondary">
                                            <div class="card-body">
                                        

                                            <ul class="list-group">
                                                <?php $jum = count($ku); for($i=0;$i<$jum;$i++){?>
                                                    <li class="list-group-item"><?= $ku[$i]['nama_matkul']?></li>
                                                <?php } ?>
                                            </ul>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="tab-pane p-3" id="messages1" role="tabpanel">
                                        <div class="card text-gray bg-white">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <h5>Minat</h5>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-minat"><i class="ti-pencil mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="ml-2" style="background-color: #f5f5f5; padding: 5px;">
                                                        Computer Engineering
                                                    </div>
                                                    <div class="ml-2" style="background-color: #f5f5f5; padding: 5px;">
                                                        Teknologi Informasi
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card text-gray bg-white">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <h5>Pengalaman</h5>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pengalaman"><i class="ti-plus mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <svg width="36" height="35" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5718 0.855014C17.0054 -0.240162 18.9946 -0.24016 20.4282 0.855016L33.7828 11.057C35.1149 12.0746 35.6712 13.8149 35.1762 15.4166L29.9957 32.181C29.4778 33.857 27.9283 35 26.1741 35H9.82595C8.07172 35 6.52218 33.857 6.00426 32.181L0.823773 15.4166C0.328847 13.8149 0.885102 12.0746 2.21721 11.057L15.5718 0.855014Z" fill="#CCD0EB"></path></svg>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <span>Test</span><br>
                                                        <span>Pertamina</span><br>
                                                        <span>Feb 2021 - Sekarang</span>
                                                        <br>
                                                        <span>Surabaya</span>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pengalaman"><i class="ti-pencil mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <svg width="36" height="35" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5718 0.855014C17.0054 -0.240162 18.9946 -0.24016 20.4282 0.855016L33.7828 11.057C35.1149 12.0746 35.6712 13.8149 35.1762 15.4166L29.9957 32.181C29.4778 33.857 27.9283 35 26.1741 35H9.82595C8.07172 35 6.52218 33.857 6.00426 32.181L0.823773 15.4166C0.328847 13.8149 0.885102 12.0746 2.21721 11.057L15.5718 0.855014Z" fill="#CCD0EB"></path></svg>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <span>Test</span><br>
                                                        <span>Pertamina</span><br>
                                                        <span>Feb 2021 - Sekarang</span>
                                                        <br>
                                                        <span>Surabaya</span>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pengalaman"><i class="ti-pencil mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                        <div class="card text-gray bg-white">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <h5>Riwayat Pendidikan</h5>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pendidikan"><i class="ti-plus mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <svg width="36" height="35" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5718 0.855014C17.0054 -0.240162 18.9946 -0.24016 20.4282 0.855016L33.7828 11.057C35.1149 12.0746 35.6712 13.8149 35.1762 15.4166L29.9957 32.181C29.4778 33.857 27.9283 35 26.1741 35H9.82595C8.07172 35 6.52218 33.857 6.00426 32.181L0.823773 15.4166C0.328847 13.8149 0.885102 12.0746 2.21721 11.057L15.5718 0.855014Z" fill="#CCD0EB"></path></svg>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <span class="badge badge-info">S2</span><br>
                                                        <span>Universitas Indonesia</span><br>
                                                        <span>M.Kom · 2021 - 2021 · Teknik Informatika</span>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pendidikan"><i class="ti-pencil mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <svg width="36" height="35" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.5718 0.855014C17.0054 -0.240162 18.9946 -0.24016 20.4282 0.855016L33.7828 11.057C35.1149 12.0746 35.6712 13.8149 35.1762 15.4166L29.9957 32.181C29.4778 33.857 27.9283 35 26.1741 35H9.82595C8.07172 35 6.52218 33.857 6.00426 32.181L0.823773 15.4166C0.328847 13.8149 0.885102 12.0746 2.21721 11.057L15.5718 0.855014Z" fill="#CCD0EB"></path></svg>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <span class="badge badge-info">S1</span><br>
                                                        <span>Universitas Indonesia</span><br>
                                                        <span>S.Kom · 2021 - 2021 · Teknik Informatika</span>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-pendidikan"><i class="ti-pencil mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                        <div class="card text-gray bg-white">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <h5>Media Sosial</h5>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#" data-toggle="modal" data-target=".modal-sosial"><i class="ti-plus mr-2 font-size-24" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tambah"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <a href="#"><i class="ti-linkedin mr-2 font-size-24"></i></a>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#"><i class="ti-facebook mr-2 font-size-24"></i></a>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="#"><i class="ti-twitter mr-2 font-size-24"></i></a>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12 p-3">
                                    <h6>Dilihat juga oleh</h6>
                                    <span>Belum dilihat</span>
                                </div>
                            </div>
                        </div>

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <!-- Modal Minat -->
                <div class="modal fade modal-minat" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">MINAT :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group mb-0">
                                        <select class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Choose ..." style="width: 480px;">
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="submit" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- Modal Pengalaman -->
                <div class="modal fade modal-pengalaman" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">PENGALAMAN :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Jabatan :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g Dosen Magister">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Jenis Pekerjaan :</label><br>
                                    <select class="form-control select2" style="width: 400px;">
                                        <option value="AK">Full-Time</option>
                                        <option value="HI">Part-Time</option>
                                        <option value="HI">Usaha</option>
                                        <option value="HI">Freelance</option>
                                        <option value="HI">Kontrak</option>
                                        <option value="HI">Magang</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Instansi / Perusahaan :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g Pertamina">
                                </div>
                                <div class="form-group">
                                    <label>Lokasi :</label>
                                    <input type="text" class="colorpicker-default form-control colorpicker-element" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g Surabaya / Indonesia">
                                </div>
                                <div class="form-group">
                                    <label>Bekerja sampai saat ini?</label><br>
                                    <input type="checkbox" id="switch3" switch="bool" onclick="myFunction()"/>
                                            <label for="switch3" data-on-label="Ya"
                                                    data-off-label="Tidak"></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tanggal Mulai :</label><br>
                                    <select class="form-control select2" style="width: 150px;">
                                        <option value="AK">Bulan</option>
                                    </select>
                                    <select class="form-control select2" style="width: 150px;">
                                        <option value="AK">Tahun</option>
                                    </select>
                                </div>
                                <div class="form-group" id="selesai" style="display: none;">
                                    <label class="control-label">Tanggal Selesai :</label><br>
                                    <select class="form-control select2" style="width: 150px;">
                                        <option value="AK">Bulan</option>
                                    </select>
                                    <select class="form-control select2" style="width: 150px;">
                                        <option value="AK">Tahun</option>
                                    </select>
                                </div>
                                
                                <div class="row">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="submit" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- Modal Pendidikan -->
                <div class="modal fade modal-pendidikan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">PENDIDIKAN :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Nama Perguruan Tinggi :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g Universitas Indonesia">
                                    </div>
                                    <br>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Jenjang :</label><br>
                                        <select class="form-control select2" style="width: 200px;">
                                            <option value="AK">Doktor</option>
                                            <option value="HI">Magister</option>
                                            <option value="HI">Sarjana</option>
                                            <option value="HI">Diploma</option>
                                            <option value="HI">SMK / SMA / MA</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Gelar :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g S.Kom">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Program Studi :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="e.g Teknik Informatika">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Tahun Mulai :</label><br>
                                        <select class="form-control select2" style="width: 150px;">
                                            <option value="AK">Tahun</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Tahun Lulus (Optional) :</label><br>
                                        <select class="form-control select2" style="width: 150px;">
                                            <option value="AK">Tahun</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                    <button type="submit" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- Modal Sosial Media -->
                <div class="modal fade modal-sosial" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">MEDIA SOSIAL :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>LinkedIn :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="Masukkan link profil linkedin anda">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Facebook :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="Masukkan link profil facebook anda">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Twitter :</label>
                                        <input type="text" class="form-control" data-colorpicker-id="1" data-original-title="" title="" placeholder="Masukkan link profil twitter anda">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                        <button type="submit" class="btn btn-secondary">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                 <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                © <script>document.write(new Date().getFullYear())</script> STIP JAKARTA
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- end main content-->
            </div>
        <!-- END layout-wrapper -->