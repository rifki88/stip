<?php
function buatkalender($tanggal,$bulan,$tahun) {      
  $bulanan=array(1=>"Januari","Februari","Maret","April",
                    "Mei","Juni","Juli","Agustus","September", 
                    "Oktober","November","Desember");
  $bln=date("n");
  $thn=date("Y");

  $jmlhari = date("t",mktime(0,0,0,$bulan,1,$tahun));
  $haritglsatu = date("w",mktime(0,0,0,$bulan,1,$tahun));

  $kalender = "<table cellspacing=0 cellpadding=1 2px  
               border=0 style='border:0px solid #ffffff; padding:0px; font-size:14px; font-family:calibri;' width=100%>\n";
  $kalender .= "<tr style='border:0px solid;'>
               <td colspan=7 style='border:0px solid;'><center>$bulanan[$bln], $thn </center>
               </td></tr>\n";

  $kalender .= "<tr class=tr_judul>
                <td><b>M</b></td><td><b>S</b></td><td><b>S</b></td><td><b>R</b></td>
                <td><b>K</b></td><td><b>J</b></td><td><b>S</b></td></tr>\n";
  $a 	  = 1;
  $adabaris   = TRUE;
  $tgl = date("d");
  $bn = date("m");
  $tn = date("Y");
  $mulaicetak = 0;
  while ($adabaris) {
    
    $kalender .= "<tr align=center class=tr_terang>";
    for ($i = 0; $i < 7; $i++ ) {
      if ($mulaicetak < $haritglsatu) {
        $kalender .= "<td>&nbsp;</td>";
        $mulaicetak++;
      } 
      elseif ($a <= $jmlhari) {
        $tt = $a;
        if ($a == $tanggal) { 
          $tt = "<a title=\"Lihat Jadwal\" style=\"text-decoration:none\" href=\"jadwalumum?id=$tn-$bn-$tt\"> <span style=\"color: white; font-weight: bold; 
                 font-size: larger; text-decoration: blink;\">
                 $tt</span></a>"; 
        }
        if ($i == 0) { 
          $tt = "<font color=\"#000\"><a style=\"color:white\" href='jadwal?id=$tn-$bn-$tt'> $tt </a></font>"; 
        }
        $kalender .= "<td bgcolor=\"#29b6f6\" ><a style=\"color:white\" href='jadwalumum?id=$tn-$bn-$a'>$tt</a></td>";
        $a++;
      } 
      else {
        $kalender .= "<td>&nbsp;</td>";
      }
    }
    $kalender .= "</tr>\n";
    if ($a <= $jmlhari) { 
      $adabaris = TRUE; 
    } 
    else { 
      $adabaris = FALSE; 
    }
  }
  $kalender .= "</table>\n";
  return $kalender;
}
?>
