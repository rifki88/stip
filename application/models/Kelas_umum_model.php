<?php

class Kelas_umum_model extends CI_Model {


public function __construct()
{
	parent::__construct();
	$this->load->helper(array('form', 'url','file'));
}




// CRUD kelas umum
   public function InsertKelas()
    {	
    	date_default_timezone_set('Asia/jakarta');
    	$tanggal_update = date('Y-m-d H:i:s');
		$nama_matkul1 = strtoupper($this->input->post('nama', true));
		$nama_matkul  = substr($nama_matkul1,0,1);
		$gabung_kode  = 'KU'.$nama_matkul;
		$cek_data     = $this->db->query("SELECT MAX(SUBSTRING(id,4)) as no_data FROM kelas_umum WHERE id LIKE '%$gabung_kode%'; ")->row();
		$cek_hasil    = $cek_data->no_data;
		//echo $nama_matkul;die();
    	if (empty($cek_data)) {
    		$kode_kelas_no_urut = '0';
    	}else{
    		$kode_kelas_no_urut = $cek_hasil+1;
    	}
    	$kode_kelas = 'KU'.$nama_matkul.$kode_kelas_no_urut;
        $data = [
			"id"             => $kode_kelas, 
			"nama_matkul"    => strtoupper($this->input->post('nama', true)), 
			"kode_kelas"     => strtoupper($this->input->post('id_kelas', true)),
			"deskripsi"      => $this->input->post('deskripsi', true),
			"type"           => $this->input->post('type', true),
			"keterangan"     => $this->input->post('keterangan', true),
			"user_update"    => $this->session->userdata('full_name'),
			"tanggal_update" => $tanggal_update,
			"status"         => "AKTIF"
        ];
        return $this->db->insert('kelas_umum', $data);
    }

    public function ListKelas()
	{
		$ses_nama = $this->session->userdata('full_name');
		// $sql = $this->db->query("select * from kelas_umum where user_update = '$ses_nama' and status = 'AKTIF';");
		$sql = $this->db->query("SELECT count(kelas.id_kelas) as jml,kelas_umum.kode_kelas, kelas_umum.id,kelas_umum.nama_matkul,kelas_umum.user_update,kelas_umum.tanggal_update FROM `kelas_umum` left join kelas on kelas_umum.kode_kelas = kelas.id_kelas where kelas_umum.status = 'AKTIF' AND  kelas_umum.user_update = '".$ses_nama."' group by kelas_umum.kode_kelas");
		$data = $sql->result_array();
		return $data;
	}

	public function hapus_kelas_umum($tanggal_update,$id,$user_update)
	{
        $hasil=$this->db->query("UPDATE kelas_umum SET status = 'NOT AKTIF', user_update = '$user_update' ,tanggal_update = '$tanggal_update' where id = '$id'");
        return $hasil;
    }

 //    public function update_kelas_umum($tanggal_update,$id,$nama,$id,$id_kelas,$deskripsi,$type,$keterangan,$user_update)
	// {
 //        $hasil=$this->db->query("UPDATE kelas_umum SET nama_matkul = '$nama' , kode_kelas = '$id_kelas' , deskripsi = '$deskripsi' , type = '$type', keterangan = '$keterangan' , user_update = '$user_update' ,tanggal_update = '$tanggal_update' where id = '$id'");
 //        return $hasil;
 //    }


 public function ambilkelas($id){
	$data =  $this->db->query("select * from kelas_umum where id = '$id'")->result_array();
	return $data;
 }

 public function memberkelas($id){
	$data =  $this->db->query("select *  from kelas where id_kelas = '$id'")->num_rows();
	return $data;
 }

 public function memberkelaslist($id){
	$data =  $this->db->query("select *  from kelas where id_kelas = '$id'")->result_array();
	return $data;
 }

 public function memberkelasakademik($id){
	$data =  $this->db->query("select *  from kelas where id_kelas = '$id'")->num_rows();
	return $data;
 }
 public function kelasumum(){
	$data =  $this->db->query("select * from kelas_umum where user_update = '".$this->session->userdata('full_name')."'")->result_array();
	return $data;
 }

  // tutup CRUD

} ?>
