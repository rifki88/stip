<?php

class Diskusi_kelas_model extends CI_Model {


public function __construct()
{
	parent::__construct();
	$this->load->helper(array('form', 'url','file'));
}



  
// CRUD kelas umum
   public function InsertInfo()
    {	
    	date_default_timezone_set('Asia/jakarta');
    	$tanggal_update = date('Y-m-d H:i:s');
        $data = [ 
			"judul"          => strtoupper($this->input->post('judul', true)), 
			"keterangan"     => $this->input->post('keterangan', true),
			"user_update"    => $this->session->userdata('full_name'),
			"tanggal_update" => $tanggal_update
        ];
        return $this->db->insert('info', $data);
    }

	public function InsertMateri()
    {	
    	date_default_timezone_set('Asia/jakarta');
    	$tanggal_update = date('Y-m-d H:i:s');
		$uploadmateri = str_replace(" ","_",$_FILES['uploadmateri']['name']);
		$tujuan_file = realpath(APPPATH.'../assets/upload/materi/');
		$konfigurasi = array(
			'allowed_types'	=> 'jpg|jpeg|png|bmp|JPG',
			'upload_path'	=> $tujuan_file,
			'remove_spaces'	=> TRUE,
			'file_name' => $uploadmateri
		);

		$this->load->library('upload',$konfigurasi);
		$this->upload->do_upload('uploadmateri');
		$this->upload->data();
        $data = [ 
			"id_kelas"       => $this->input->post('id_kelas', true), 
			"id_sesi"       => $this->input->post('id_sesi', true), 
			"judul"          => strtoupper($this->input->post('judul', true)), 
			"berkas"          => $uploadmateri, 
			"keterangan"     => $this->input->post('keterangan', true),
			"user_update"    => $this->session->userdata('full_name'),
			"tanggal_update" => $tanggal_update,
			"status"         => "AKTIF"
        ];
        return $this->db->insert('materi', $data);
    }

	public function InsertTugas()
    {	
    	date_default_timezone_set('Asia/jakarta');
    	$tanggal_update = date('Y-m-d H:i:s');

		 $berkas = str_replace(" ","_",$_FILES['berkas']['name']);
			$tujuan_file = realpath(APPPATH.'../assets/images/upload/');
			$konfigurasi = array(
				'allowed_types'	=> 'jpg|jpeg|png|bmp|JPG',
				'upload_path'	=> $tujuan_file,
				'remove_spaces'	=> TRUE,
				'file_name' => $berkas
			);

		$this->load->library('upload',$konfigurasi);
		$this->upload->do_upload('berkas');
		$this->upload->data();


        $data = [ 
			"id_sesi"  		=> $this->input->post('id_sesi', true),
			"id_kelas"  	=> $this->input->post('id_kelas', true),
			"judul"          => strtoupper($this->input->post('judul', true)), 
			"keterangan"     => $this->input->post('keterangan', true),
			"berkas"		=> $berkas,
			"user_update"    => $this->session->userdata('full_name'),
			"tanggal_update" => $tanggal_update
        ];

		return $this->db->insert('tugas', $data);

    }

    public function ListKelas()
	{
		$ses_nama = $this->session->userdata('full_name');
		$sql = $this->db->query("select * from kelas_umum where user_update = '$ses_nama' and status = 'AKTIF';");
		$data = $sql->result_array();
		return $data;
	}

	public function hapus_kelas_umum($tanggal_update,$id,$user_update)
	{
        $hasil=$this->db->query("UPDATE kelas_umum SET status = 'NOT AKTIF', user_update = '$user_update' ,tanggal_update = '$tanggal_update' where id = '$id'");
        return $hasil;
    }

	public function getsesi()
	{
		$sql = $this->db->query("select * from sesi ");
		$data = $sql->result_array();
		return $data;
}


 //    public function update_kelas_umum($tanggal_update,$id,$nama,$id,$id_kelas,$deskripsi,$type,$keterangan,$user_update)
	// {
 //        $hasil=$this->db->query("UPDATE kelas_umum SET nama_matkul = '$nama' , kode_kelas = '$id_kelas' , deskripsi = '$deskripsi' , type = '$type', keterangan = '$keterangan' , user_update = '$user_update' ,tanggal_update = '$tanggal_update' where id = '$id'");
 //        return $hasil;
 //    }




  // tutup CRUD

} ?>
