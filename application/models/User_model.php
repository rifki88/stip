<?php

class User_model extends CI_Model {


public function __construct()
{
	parent::__construct();
	$this->load->helper(array('form', 'url','file'));
}


public function login($email,$password)
	{
		$sql = $this->db->query("select * from user where email='".$email."' and password='".$password."'");
		$data = $sql->result_array();
		return $data;
}

public function proses_registrasi(){
	$data = array(
		'email'=>$this->input->post('email'),
		'password'=>md5($this->input->post('password')),
		'full_name'=>$this->input->post('full_name'),
		'access'=>'siswa'
	);
	$this->db->insert('user',$data);
}

public function informasi_kelas($t)
	{
		$sql = $this->db->query("select * from kelas_umum where id='$t' limit 1");
		$data = $sql->result_array();

		return $data;
}

public function getallmateri()
	{
		$sql = $this->db->query("select * from kelas_umum where user_update = '".$this->session->userdata('full_name')."'");
		$data = $sql->result_array();
		$array = [];
		for($i=0;$i<count($data);$i++){
			$array[$i] = array(
				"id"=>$data[$i]['id'],
				"kode_kelas"=>$data[$i]['kode_kelas'],
				"nama_matkul"=>$data[$i]['nama_matkul'],
				"type"=>$data[$i]['type'],
				"deskripsi"=>$data[$i]['deskripsi'],
				"keterangan"=>$data[$i]['keterangan'],
				"status"=>$data[$i]['status'],
				"user_update"=>$data[$i]['user_update'],
				"tanggal_update"=>$data[$i]['tanggal_update'],
				"thumbnail"=>$data[$i]['thumbnail'],
				"cover"=>$data[$i]['cover'],
				"komentar"=>$this->db->query("select * from komentar where id_relasi = '".$data[$i]['id']."'")->result_array()
			);
		}
		return $array;
}


public function proses_add_komentar($id){
$tanggal = date("Y-m-d H:i:s");
	$data = array(
		'id_relasi'=>$id,
		'komentar'=>$this->input->post('komentar'),
		'suka'=>1,
		'user_update'=>$this->session->userdata('full_name'),
		'tanggal_update'=>$tanggal,
	);
	$this->db->insert('komentar',$data);
}

public function proses_add_komentarpost($id){
	$tanggal = date("Y-m-d H:i:s");
		$data = array(
			'id_post'=>$id,
			'komentar'=>$this->input->post('komentar'),
			'suka'=>1,
			'user_update'=>$this->session->userdata('full_name'),
			'tanggal_update'=>$tanggal,
		);
		$this->db->insert('komentar',$data);
	}



public function proses_update_profile(){
		$id = $this->session->userdata('id_user');
		$data = array(
			'email'=>$this->input->post('email'),
			'full_name'=>$this->input->post('full_name'),
			'phone'=>$this->input->post('phone'),
			'gender'=>$this->input->post('gender')
		);
		$this->db->where('id_user',$id);
		$this->db->update('user',$data);
	}


	public function select_profile($id)
	{
		$sql = $this->db->query("select * from user where id_user='$id'");
		$data = $sql->result_array();
		return $data;
	}


	
public function proses_update_password(){
	$id = $this->session->userdata('id_user');
	$row = $this->db->query("select * from user where password = '".md5($this->input->post('pass1'))."' and id_user='$id'")->num_rows();
	if($row > 0){
		if($this->input->post('pass2') == $this->input->post('conf')){
			$data = array(
				'password'=>md5($this->input->post('pass2')),
				);
			$this->db->where('id_user',$id);
			$this->db->update('user',$data);
		}else{
			echo "password tidak cocok";
		}
	}else{
		echo "password tidak ada";
	}

}


	
public function getkontak(){
	
	$sql = $this->db->query("select * from user where access = 'siswa'");
	$data = $sql->result_array();
	return $data;

}



public function getalluser(){
	
	$sql = $this->db->query("select * from user ");
	$data = $sql->result_array();
	return $data;

}



public function getallkelasakademik(){
	$ses_nama = $this->session->userdata('full_name');
	$sql = $this->db->query("SELECT count(kelas.id_kelas) as jml,kelas_akademik.kode_kelas, kelas_akademik.id,kelas_akademik.nama_kelas,kelas_akademik.user_update FROM `kelas_akademik` left join kelas on kelas_akademik.kode_kelas = kelas.id_kelas where kelas_akademik.user_update = '".$ses_nama."' group by kelas_akademik.kode_kelas");
	$data = $sql->result_array();
	return $data;

}




public function getalljadwal(){
	
	$sql = $this->db->query("select * from jadwal order by id DESC");
	$data = $sql->result_array();
	return $data;

}


public function getjadwalpertanggal($tgl){
	
	$sql = $this->db->query("select * from jadwal where tanggal = '".$tgl."' order by id DESC");
	$data = $sql->result_array();
	return $data;

}



public function getallkelasakademikdetail($id){
	
	$sql = $this->db->query("select * from kelas_akademik where id = '".$id."' ");
	$data = $sql->result_array();
	return $data;

}




public function getsearchkelasakademik(){
	
	$nama_kelas = $this->input->post('nama_kelas');
	$idperiode = $this->input->post('idperiode');
	
	$sql = $this->db->query("select * from kelas_akademik where nama_kelas LIKE '%".$nama_kelas."%' and idperiode LIKE '".$idperiode."' ");
	$data = $sql->result_array();
	return $data;

}



public function getallsesi(){
	
	$sql = $this->db->query("select * from sesi ");
	$data = $sql->result_array();
	return $data;

}


public function getallsesiakademik(){
	
	$sql = $this->db->query("select * from sesi where type = '2' and user_update = '".$this->session->userdata('full_name')."' order by id DESC");
	$data = $sql->result_array();
	return $data;

}


public function getalltugas(){
	
	$sql = $this->db->query("select * from tugas ");
	$data = $sql->result_array();
	return $data;

}


public function getallquiz(){
	
	$sql = $this->db->query("select * from quiz ");
	$data = $sql->result_array();
	return $data;

}



public function save_sesi(){
	$data = array(
		'tujuan'=>$this->input->post('tujuan'),
		'nama_sesi'=>$this->input->post('nama_sesi'),
		'id_kelas'=>$this->input->post('id_kelas'),
		'tanggal'=>$this->input->post('tanggal'),
		'waktu_mulai'=>$this->input->post('waktu_mulai'),
		'waktu_akhir'=>$this->input->post('waktu_akhir'),
		'tipe_sesi'=>$this->input->post('tipe_sesi'),
		'user_update'=>$this->session->userdata('full_name'),
		'tanggal_update'=>date("Y-m-d H:i:s"),
		'status'=>1,
	);
	$this->db->insert('sesi',$data);
}



public function delete_sesi($id)
	{
		$sql = $this->db->query("delete from sesi where id = '".$id."' ");
		return $sql;
}



public function save_sesi_akademik(){
	$data = array(
		'tujuan'=>$this->input->post('tujuan'),
		'nama_sesi'=>$this->input->post('nama_sesi'),
		'id_kelas'=>$this->input->post('id_kelas'),
		'tanggal'=>$this->input->post('tanggal'),
		'waktu_mulai'=>$this->input->post('waktu_mulai'),
		'waktu_akhir'=>$this->input->post('waktu_akhir'),
		'type'=>$this->input->post('type'),
		'user_update'=>$this->session->userdata('full_name'),
		'tanggal_update'=>date("Y-m-d H:i:s"),
		'status'=>1,
	);
	$this->db->insert('sesi',$data);
}





public function save_jadwal(){
	$data = array(
		'nama_jadwal'=>$this->input->post('nama_jadwal'),
		'keterangan'=>$this->input->post('keterangan'),
		'tanggal'=>$this->input->post('tanggal'),
		'user_update'=>$this->session->userdata('full_name'),
		'tanggal_update'=>date("Y-m-d H:i:s")
	);
	$this->db->insert('jadwal',$data);
}



public function delete_jadwal($id)
	{
		$sql = $this->db->query("delete from jadwal where id = '".$id."' ");
		return $sql;
}




public function delete_kelasumum($id)
	{
		$sql = $this->db->query("delete from kelas_umum where id = '".$id."' ");
		return $sql;
}


public function delete_kelas($id)
	{
		$sql = $this->db->query("delete from kelas where id = '".$id."' ");
		return $sql;
}



public function update_kelas(){
	$data = array(
		'id_kelas'=>$this->input->post('id_kelas'),
		'nama'=>$this->input->post('nama'),
		'type'=>$this->input->post('type'),
		'deskripsi'=>'Kelas '.$this->input->post('deskripsi'),
		'keterangan'=> 'Kelas '.$this->input->post('keterangan') ,
		'status'=>'AKTIF',
		'user_update'=>$this->session->userdata('full_name')
	);
	$this->db->where('id',$this->input->post('id'));
	$this->db->update('kelas',$data);
}


public function selectkelasumum($id)
	{
		$sql = $this->db->query("select * from kelas_umum where id = '".$id."' ")->result_array();
		return $sql;
}


public function update_kelasumum(){

	$foto_thumbnail = str_replace(" ","_",$_FILES['thumbnail']['name']);
	$foto_cover = str_replace(" ","_",$_FILES['cover']['name']);
	if(!empty($foto_thumbnail) || !empty($foto_cover)){
	$tujuan_file = realpath(APPPATH.'../assets/upload/thumbnail/');
	$konfigurasi = array(
		'allowed_types'	=> 'jpg|jpeg|png',
		'upload_path'	=> $tujuan_file,
		'remove_spaces'	=> TRUE
	);

	$this->load->library('upload',$konfigurasi);
	$this->upload->do_upload('thumbnail');
	$this->upload->do_upload('cover');
	$this->upload->data();
	
			
			$data = array(
				'nama_matkul'=>$this->input->post('nama_matkul'),
				'type'=>$this->input->post('type'),
				'deskripsi'=>$this->input->post('deskripsi'),
				'keterangan'=> $this->input->post('keterangan') ,
				'thumbnail'=>  $foto_thumbnail,
				'cover'=>  $foto_cover,

			);
			$this->db->where('id',$this->input->post('id'));
			$this->db->update('kelas_umum',$data);
	

	}else{

		$data = array(
			'nama_matkul'=>$this->input->post('nama_matkul'),
			'type'=>$this->input->post('type'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'keterangan'=> $this->input->post('keterangan') 
		);
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('kelas_umum',$data);

	}
	
}



public function selectkelas($id)
	{
		$sql = $this->db->query("select * from kelas where id = '".$id."' ")->result_array();
		return $sql;
}

public function selectpengguna($id)
{
		$sql = $this->db->query("select * from user where id_user = '".$id."' ")->result_array();
		return $sql;
}

public function selectpenggunanama($id)
{
		$sql = $this->db->query("select * from user where full_name = '".$id."' ")->result_array();
		return $sql;
}


public function update_user(){
	$data = array(
		'password'=>md5($this->input->post('password')),
		'full_name'=>$this->input->post('full_name'),
		'phone'=>$this->input->post('phone'),
		'gender'=>$this->input->post('gender'),
		'access'=>$this->input->post('access')
	);
	$this->db->where('id_user',$this->input->post('id_user'));
	$this->db->update('user',$data);
}




public function getallkelas()
	{
		$sql = $this->db->query("select * from kelas ");
		$data = $sql->result_array();
		return $data;
}



public function delete_user($id)
	{
		$sql = $this->db->query("delete from user where id_user = '".$id."' ");
		return $sql;
}

public function getallkelasumum()
	{
		$sql = $this->db->query("select * from kelas_umum ");
		$data = $sql->result_array();
		return $data;
}


public function save_gabungkelas(){
	$data = array(
		'id_kelas'=>$this->input->post('pin'),
		'nama'=>'Kelas '.$this->input->post('pin'),
		'type'=>'Publik',
		'deskripsi'=>'Kelas '.$this->input->post('pin'),
		'keterangan'=> 'Kelas '.$this->input->post('pin') ,
		'status'=>'AKTIF',
		'user_update'=>$this->session->userdata('full_name')
	);
	$this->db->insert('kelas',$data);
}



public function save_gabungkelasakademik(){
	$data = array(
		'id_kelas'=>$this->input->post('pin'),
		'nama'=>'Kelas '.$this->input->post('pin'),
		'type'=>'Publik',
		'deskripsi'=>'Kelas '.$this->input->post('pin'),
		'keterangan'=> 'Kelas '.$this->input->post('pin') ,
		'status'=>'AKTIF',
		'user_update'=>$this->session->userdata('full_name')
	);
	$this->db->insert('kelas',$data);
}

public function save_quiz(){
	$data = array(
		'id_kelas'=>$this->input->post('id_kelas'),
		'id_sesi'=>$this->input->post('id_sesi'),
		'judul'=>$this->input->post('judul')
	);
	$this->db->insert('quiz',$data);
	$insert_id = $this->db->insert_id();
   	return  $insert_id;
}


public function select_question($id)
	{
		$sql = $this->db->query("select * from quiz where id = '".$id."' ")->result_array();
		return $sql;
}


public function proses_save_quiz($id){

	$data = array(
		'judul_quiz'=>$this->input->post('judul_quiz'),
		'jam_pengerjaan'=>$this->input->post('batas_menit'),
		'indikator_nilai'=>$this->input->post('indikator_penilaian'),
		'batas_nilai'=>$this->input->post('batas_nilai'),
		'batas_menit'=>$this->input->post('batas_menit') ,
		'batas_waktu'=>$this->input->post('batas_waktu') ,
		'status_laporan'=>$this->input->post('laporan') ,
		'tanggal_dibagikan'=>$this->input->post('tanggal_dibagikan') ,
		'catatan'=>$this->input->post('catatan') ,
		'tanggal_update'=>date("Y-m-d H:i:s"),
		'user_update'=>$this->session->userdata('full_name')
	);
	$this->db->where('id',$id);
	$this->db->update('quiz',$data);

}



public function proses_save_question($id){

	$foto_soal = str_replace(" ","_",$_FILES['foto_soal']['name']);
	$tujuan_file = realpath(APPPATH.'../assets/upload/quiz/');
	$konfigurasi = array(
		'allowed_types'	=> 'jpg|jpeg|png|bmp|JPG',
		'upload_path'	=> $tujuan_file,
		'remove_spaces'	=> TRUE,
		'file_name' => $foto_soal
	);

	$this->load->library('upload',$konfigurasi);
	$this->upload->do_upload('foto_soal');
	$this->upload->data();


	$data = array(
		'id_quiz'=>$id,
		'soal'=>$this->input->post('soal'),
		'pil_a'=>$this->input->post('pil_a'),
		'pil_b'=>$this->input->post('pil_b'),
		'pil_c'=>$this->input->post('pil_c'),
		'pil_d'=>$this->input->post('pil_d') ,
		'pil_e'=>$this->input->post('pil_e') ,
		'foto' => $foto_soal,
		'jawaban'=>$this->input->post('jawaban') ,
		'tanggal_update'=>date("Y-m-d H:i:s"),
		'user_update'=>$this->session->userdata('full_name')
	);
	$this->db->insert('quiz_detail',$data);


}

public function getidkelas($id){

	$return = $this->db->query("select * from quiz where id = '".$id."'")->result_array();
	return $return;

}

public function save_post(){


	$foto = str_replace(" ","_",$_FILES['foto']['name']);
	if(!empty($foto)){
	$tujuan_file = realpath(APPPATH.'../assets/upload/post/');
	$konfigurasi = array(
		'allowed_types'	=> 'jpg|jpeg|png',
		'upload_path'	=> $tujuan_file,
		'remove_spaces'	=> TRUE
	);

	$this->load->library('upload',$konfigurasi);
	$this->upload->do_upload('foto');
	$this->upload->data();

	$data = array(
		'description'=>$this->input->post('description'),
		'foto'=>$foto,
		'tanggal_update'=>date("Y-m-d H:i:s"),
		'user_update'=>$this->session->userdata('full_name')

	);
	$this->db->insert('post',$data);

}else{

$data = array(
	'description'=>$this->input->post('description'),
	'tanggal_update'=>date("Y-m-d H:i:s"),
	'user_update'=>$this->session->userdata('full_name')

);
$this->db->insert('post',$data);
}
}


public function getallpost()
	{
		$sql = $this->db->query("select * from post");
		$data = $sql->result_array();
		$array = [];
		for($i=0;$i<count($data);$i++){
			$array[$i] = array(
				"id_post"=>$data[$i]['id_post'],
				"foto"=>$data[$i]['foto'],
				"description"=>$data[$i]['description'],
				"tanggal_update"=>$data[$i]['tanggal_update'],
				"user_update"=>$data[$i]['user_update'],
				"komentar"=>$this->db->query("select * from komentar where id_post = '".$data[$i]['id_post']."'")->result_array()
			);
		}
		return $array;
}


public function getallfiles()
	{
		$sql = $this->db->query("select * from materi where user_update = '".$this->session->userdata('full_name')."'");
		$data = $sql->result_array();
		return $data;
}


public function getdetailkontak($id)
	{
		$sql = $this->db->query("select * from user where id_user = '".$id."'");
		$data = $sql->result_array();
		return $data;
}

public function save_chat(){

$data = array(
	'dari'=>$this->input->post('dari'),
	'tujuan'=>$this->input->post('tujuan'),
	'isi'=>$this->input->post('isi'),
	'tanggal_update'=>date("Y-m-d H:i:s"),
	'user_update'=>$this->session->userdata('full_name')

);
$this->db->insert('pesan',$data);
}

} ?>
