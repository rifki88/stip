<?php

class User extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->helper(array('form'));
  }



	public function dashboard() {
    
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $t['access'] = $this->session->userdata('access');
    $this->load->model('User_model','materi');
    $this->load->model('Kelas_umum_model','kelasumum');
    $t['post'] = $this->materi->getallpost();
    $t['ku'] = $this->kelasumum->kelasumum();  
    $t['tugas'] = $this->materi->getalltugas();  
    $t['ak'] = $this->materi->getallkelasakademik();
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('dashboard/content',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  
	public function ruangkelas() {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    //$url = file_get_contents('http://lms-backend.mamorasoft.com/api/list-kelas?page=1',true);
    //$t['api'] = json_decode($url,true);
    //$url = file_get_contents(base_url('assets/json/kelas.json'),true);
    //$t['api'] = json_decode($url,true);
    // $ju = count($t['api']['list_kelas']);
    // for($i=0;$i<$ju;$i++){
    //   $this->db->query("insert into kelas_akademik(kode_kelas,nama_kelas,daya_tampung,idperiode,idunit,idkurikulum,idmk)values(
    //     '".$t['api']['list_kelas'][$i]['idkelas']."',
    //     '".$t['api']['list_kelas'][$i]['namakelas']."',
    //     '".$t['api']['list_kelas'][$i]['dayatampung']."',
    //     '".$t['api']['list_kelas'][$i]['idperiode']."',
    //     '".$t['api']['list_kelas'][$i]['idunit']."',
    //     '".$t['api']['list_kelas'][$i]['idkurikulum']."',
    //     '".$t['api']['list_kelas'][$i]['idmk']."'
    //   )");
    // }
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademik();
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('ruangkelas/content',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  
  
	public function search() {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getsearchkelasakademik();
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('ruangkelas/search',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }
  
	public function detailkelasakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $this->load->model('Kelas_umum_model','member');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['member'] = $this->member->memberkelasakademik($t['api'][0]['kode_kelas']);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('ruangkelas/detailkelasakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  
	public function diskusiakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('diskusi_kelas/diskusiakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }


  
  
	public function sesiakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['data'] = $this->akademik->getallsesiakademik();
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('sesi_kelas/sesiakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  
	public function tugasakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('tugas/tugasakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }


	public function quizakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('quiz/quizakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  

	public function berkasakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('berkas/berkasakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }


  
	public function anggotaakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('anggota/anggotaakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }


	public function laporanakademik($id) {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','akademik');
    $t['api'] = $this->akademik->getallkelasakademikdetail($id);
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('laporan/laporanakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }


  
  
	public function joinkelasakademik() {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('ruangkelas/gabungkelasakademik',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }



	public function obrolan() {
    $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $t['access'] = $this->session->userdata('access');
    $this->load->model('User_model','kontak');
    $t['data'] = $this->kontak->getkontak();
    $a['header'] =  $this->load->view('layout/header',$t, true);
    $a['footer'] =  $this->load->view('layout/footer',null, true);
    $a['content'] =  $this->load->view('obrolan/content',$t, true);
    $page = $this->load->view('layout',$a);
    return $page;
    }else{
      redirect(base_url());
    }
  }

  
	public function jelajah() {
	  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $this->load->model('User_model','files');
    $t['files'] = $this->files->getallfiles();
    $t['access'] = $this->session->userdata('access');
	  $a['header'] =  $this->load->view('layout/header',$t, true);
	  $a['footer'] =  $this->load->view('layout/footer',null, true);
	  $a['content'] =  $this->load->view('jelajah/content',$t, true);
	  $page = $this->load->view('layout',$a);
	  return $page;
      }else{
          redirect(base_url());
      }
  }

  
	public function obrolandetail() {
    $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
    $id = $this->uri->segment(3);
    $t['access'] = $this->session->userdata('access');
    $this->load->model('User_model','kontak');
    $t['data'] = $this->kontak->getkontak();
    $t['detail'] = $this->kontak->getdetailkontak($id);
    $a['header'] =  $this->load->view('layout/header',$t, true);
    $a['footer'] =  $this->load->view('layout/footer',null, true);
    $a['content'] =  $this->load->view('obrolan/detail',$t, true);
    $page = $this->load->view('layout',$a);
    return $page;
    }else{
      redirect(base_url());
    }
  }


  public function profile() {
	$t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $t['access'] = $this->session->userdata('access');
  $id = $this->session->userdata('id_user');
  $this->load->model('User_model','profile');
  $t['profile'] = $this->profile->select_profile($id);
	$a['header'] =  $this->load->view('layout/header',$t, true);
	$a['footer'] =  $this->load->view('layout/footer',null, true);
	$a['content'] =  $this->load->view('profile/content',$t, true);
	$page = $this->load->view('layout',$a);
	return $page;
	}else{
		redirect(base_url());
	}
}

 public function profile_umum() {
  $t['info'] = $this->session->userdata('full_name');
    if($t['info'] == TRUE){
  $t['access'] = $this->session->userdata('access');
  $name = $this->input->get('nama_user');
  $ganti = str_replace("-"," ",$name);
  $this->load->model('User_model','profile');
  $this->load->model('Kelas_umum_model','ku');
  $t['profile'] = $this->profile->selectpenggunanama($ganti);
  $t['materi'] = $this->profile->getallmateri();
  $t['total'] = count($t['materi']);
  $t['ku'] = $this->ku->kelasumum();
  $a['header'] =  $this->load->view('layout/header',$t, true);
  $a['footer'] =  $this->load->view('layout/footer',null, true);
  $a['content'] =  $this->load->view('profile_umum/content',$t, true);
  $page = $this->load->view('layout',$a);
  return $page;
  }else{
    redirect(base_url());
  }
}

public function logout ()
  {
      $this->session->sess_destroy();
      redirect(base_url());
  }


public function informasi_kelas()
{
  $t['id'] = $this->input->get('id');
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('informasi_kelas/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}

public function diskusi_kelas()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $this->load->model('Diskusi_kelas_model','getsesi');
  $this->load->model('User_model','User_model');
  $t['materi'] = $this->User_model->getallmateri();
  $t['sesi'] = $this->getsesi->getsesi();
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('diskusi_kelas/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}

public function sesi_kelas()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $this->load->model('User_model','sesi');
  $t['data'] = $this->sesi->getallsesi();  
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('sesi_kelas/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function tugas()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $this->load->model('User_model','tugas');
  $t['data'] = $this->tugas->getalltugas(); 
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('tugas/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function quiz()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $this->load->model('User_model','quiz');
  $t['data'] = $this->quiz->getallquiz();
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('quiz/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function berkas()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $this->load->model('User_model','files');
  $t['files'] = $this->files->getallfiles();
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['id']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('berkas/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function anggota()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['ambil'][0]['kode_kelas']);
  $t['memberkelaslist'] = $this->ambil_kelas->memberkelaslist($t['ambil'][0]['kode_kelas']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('anggota/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}




public function laporan()
{
  $t['id'] = urlencode(base64_decode($this->input->get('id')));
  $this->load->model('User_model','User_model');
  if($t == TRUE){
  $t['access'] = $this->session->userdata('access');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['id']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['id']);
  $t['data_isi'] =  $this->User_model->informasi_kelas($t['id']);
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('laporan/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}




public function pengguna()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','pengguna');
  $t['data'] = $this->pengguna->getalluser(); 
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/pengguna',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function sesi()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','sesi');
  $t['data'] = $this->sesi->getallsesi();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/sesi',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function add_sesi()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','sesi');
  $t['data'] = $this->sesi->getallsesi();  
  $t['kelas'] = $this->sesi->getallkelasumum();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/add_sesi',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function insert_sesi()
{
  $this->load->model('User_model','sesi');
  $this->sesi->save_sesi();
  redirect(base_url('user/sesi'));
}

public function delete_sesi()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','sesi');
  $this->sesi->delete_sesi($id);
  redirect(base_url('user/sesi'));
}



public function jadwalumum()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $tgl = $this->input->get('id');
  $this->load->model('User_model','jadwal');
  $t['data'] = $this->jadwal->getjadwalpertanggal($tgl);  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('jadwal/content',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}


public function jadwal()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','jadwal');
  $t['data'] = $this->jadwal->getalljadwal();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/jadwal',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function add_jadwal()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','jadwal');
  $t['kelas'] = $this->jadwal->getallkelasakademik();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/add_jadwal',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function insert_jadwal()
{
  $this->load->model('User_model','jadwal');
  $this->jadwal->save_jadwal();
  redirect(base_url('user/jadwal'));
}

public function delete_jadwal()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','jadwal');
  $this->jadwal->delete_jadwal($id);
  redirect(base_url('user/jadwal'));
}




public function add_sesi_akademik()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $id = $this->uri->segment(3);
  $this->load->model('User_model','sesi');
  $t['data'] = $this->sesi->getallsesi();  
  $t['kelas'] = $this->sesi->getallkelasakademik();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/add_sesi_akademik',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function insert_sesi_akademik()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','sesi_akademik');
  $this->sesi_akademik->save_sesi_akademik();
  redirect(base_url('user/sesiakademik/'.$id));
}



public function delete_user()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','user');
  $this->user->delete_user($id);
  redirect(base_url('user/pengguna'));
}


public function komentar(){

  $id = $this->uri->segment(3);
  $ids = $this->input->post('ids');
  $this->load->model('User_model','komentar');
  $this->komentar->proses_add_komentar($id);
  redirect(base_url('user/diskusi_kelas?id='.$ids));
}

public function komentarpost(){

  $id = $this->uri->segment(3);
  $this->load->model('User_model','komentarpost');
  $this->komentarpost->proses_add_komentarpost($id);
  redirect(base_url('user/dashboard'));
}



public function update_profile(){

  $id = $this->session->userdata('id_user');
  $this->load->model('User_model','update_profile');
  $this->update_profile->proses_update_profile();
  redirect(base_url('user/profile'));
}




public function update_password(){
  $this->load->model('User_model','update_password');
  $this->update_password->proses_update_password();
  redirect(base_url('user/dashboard'));
}



public function kelas()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','kelas');
  $t['data'] = $this->kelas->getallkelas();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/kelas',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}




public function kelasumum()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $this->load->model('User_model','kelasumum');
  $t['data'] = $this->kelasumum->getallkelasumum();  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/kelasumum',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}

public function gabungkelas(){
  $this->load->model('User_model','gabungkelas');
  $this->gabungkelas->save_gabungkelas();
  echo" Sukses Gabung Kelas";
}


public function insert_join_kelas(){
  $this->load->model('User_model','insert_join_kelas');
  $this->insert_join_kelas->save_gabungkelasakademik();
  redirect(base_url('user/ruangkelas'));
}


public function edit_kelasumum()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $id = $this->uri->segment(3);
  $this->load->model('User_model','kelasumum');
  $t['data'] = $this->kelasumum->selectkelasumum($id);  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/editkelasumum',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}





public function edit_kelas()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $id = $this->uri->segment(3);
  $this->load->model('User_model','kelas');
  $t['data'] = $this->kelas->selectkelas($id);  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/editkelas',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function proses_editkelas()
{
  $this->load->model('User_model','kelas');
  $this->kelas->update_kelas($id);
  redirect(base_url('user/kelas'));
}





public function edit_pengguna()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $id = $this->uri->segment(3);
  $this->load->model('User_model','pengguna');
  $t['data'] = $this->pengguna->selectpengguna($id);  
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('master/edituser',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}



public function proses_editpengguna()
{
  $this->load->model('User_model','user');
  $this->user->update_user($id);
  redirect(base_url('user/pengguna'));
}

public function update_kelasumum()
{
  $this->load->model('User_model','kelasumum');
  $this->kelasumum->update_kelasumum();
  redirect(base_url('user/kelasumum'));
}




public function delete_kelasumum()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','kelasumum');
  $this->kelasumum->delete_kelasumum($id);
  redirect(base_url('user/kelasumum'));
}


public function delete_kelas()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','kelas');
  $this->kelas->delete_kelas($id);
  redirect(base_url('user/kelas'));
}

public function insert_quiz()
{
  $this->load->model('User_model','quiz');
  $id = $this->quiz->save_quiz();
  if($id){
    redirect(base_url('user/add_question/'.$id));  
  }else{
    return false;
  }
}



public function add_question()
{
  $t['info'] = $this->session->userdata('full_name');
  if($t['info'] == TRUE){
  $id = $this->uri->segment(3);
  $this->load->model('User_model','question');
  $this->load->model('Kelas_umum_model','ambil_kelas');
  $t['data'] = $this->question->select_question($id);  
  $t['ambil'] = $this->ambil_kelas->ambilkelas($t['data'][0]['id_kelas']);
  $t['memberkelas'] = $this->ambil_kelas->memberkelas($t['data'][0]['id_kelas']);
  $t['access'] = $this->session->userdata('access');
  $a['header']   =  $this->load->view('layout/header',$t, true);
  $a['footer']   =  $this->load->view('layout/footer',null, true);
  $a['content']  =  $this->load->view('quiz/form',$t, true);
  
  $this->load->view('layout',$a);
  }else{
    redirect(base_url());
  }
}

public function save_quiz()
{
  $id = $this->uri->segment(3);
  $this->load->model('User_model','question');
  $this->question->proses_save_quiz($id);
  $this->question->proses_save_question($id);
  $id = $this->question->getidkelas($id);
  redirect(base_url('user/informasi_kelas?id='.$id[0]['id_kelas']));
}


public function post()
{
  $this->load->model('User_model','post');
  $this->post->save_post();
  redirect(base_url('user/dashboard'));
}





public function sent()
{
  $this->load->model('User_model','chat');
  $this->chat->save_chat();
  redirect(base_url('user/obrolan'));
}

  // tutup CRUD

	
}