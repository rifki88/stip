<?php

class Kelas_umum extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url','file'));
		$this->load->library('session');
  }

  // CRUD kelas umum
   public function SaveKelas()
    {
    	$this->load->model('Kelas_umum_model','Kelas_umum_model');
        $data = $this->Kelas_umum_model->InsertKelas();
        echo json_encode($data);
    }

    public function ListKelas()
    {
    	$this->load->model('Kelas_umum_model','Kelas_umum_model');
        $data = $this->Kelas_umum_model->ListKelas();
        echo json_encode($data);
    }

    public function GetKelas()
    {
      $id=$this->input->get('id');
      $data =  $this->db->query("select * from kelas_umum where id = '$id';")->row();
      echo json_encode($data);
    }

     public function UbahKelas()
    {
      date_default_timezone_set('Asia/jakarta');
      $tanggal_update = date('Y-m-d H:i:s');
      $nama        = strtoupper($this->input->post('nama', true)); 
      $id          = $this->input->post('id', true); 
      $id_kelas    = strtoupper($this->input->post('id_kelas', true));
      $deskripsi   = $this->input->post('deskripsi', true);
      $type        = $this->input->post('type', true);
      $keterangan  = $this->input->post('keterangan', true);
      $user_update = $this->session->userdata('full_name');
      //print_r($this->input->post());die();
      $this->load->model('Kelas_umum_model','Kelas_umum_model');
      $data = $this->Kelas_umum_model->update_kelas_umum($tanggal_update,$id,$nama,$id,$id_kelas,$deskripsi,$type,$keterangan,$user_update);
      echo json_encode($data);
    }

     public function HapusKelas()
    {
      date_default_timezone_set('Asia/jakarta');
      $tanggal_update = date('Y-m-d H:i:s');
      $id          = $this->input->post('id', true); 
      $user_update = $this->session->userdata('full_name');
      //print_r($this->input->post());die();
      $this->load->model('Kelas_umum_model','Kelas_umum_model');
      $data = $this->Kelas_umum_model->hapus_kelas_umum($tanggal_update,$id,$user_update);
      echo json_encode($data);
    }



  // tutup CRUD

	
}
